package com.ebr.testCreateStationApi;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses({ TestCreateStationBlackBox.class, TestCreateStationWhiteBox.class})
public class TestCreateStation {

}
