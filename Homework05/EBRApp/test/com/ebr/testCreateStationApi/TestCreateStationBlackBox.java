package com.ebr.testCreateStationApi;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ebr.bean.Station;
import com.ebr.serverapi.StationApi;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;


@RunWith(Parameterized.class)
public class TestCreateStationBlackBox {
	private String name;
	private String address;
	private int emptyDock;
	private String res;
	
	public TestCreateStationBlackBox(String name, String address, int emptyDock, String res) {
		super();
		this.name = name;
		this.address = address;
		this.emptyDock = emptyDock;
		this.res = res;
	}
	
	@Parameterized.Parameters
	public static Collection<Object[]> Statics() {
		return Arrays.asList(new Object[][] { 
			{ "", "Ha Noi", 5, "Can not create empty field !" },
			{ "Hello", "", 5, "Can not create empty field !" }, 
			{ "Cau Giay Station", "Ha Noi", 5,"Station exist. Add station faile"  }, 
			{ "Cau Giay", "Cau Giay", 5,"Station exist. Add station faile"  }, 
			{ "HBT Station", "Hai Ba Trung", 5, "Add station success ! "  }, 
		});
	}
	
	@Test
	public void testCreateStation() {
		System.out.println("helllooo");
		Station  station = new Station();
		StationApi api = new StationApi();
		station.setName(name);
		station.setAddress(address);
		station.setNumberOfEmptyDocks(emptyDock);
		String result = api.addStation(station);
		System.out.println("result " + result + " " + res);
		assertEquals(result,res);
	}
}
