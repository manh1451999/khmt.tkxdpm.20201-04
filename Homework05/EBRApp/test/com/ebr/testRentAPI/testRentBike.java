package com.ebr.testRentAPI;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ebr.bean.Station;
import com.ebr.serverapi.RentAPI;
import com.ebr.serverapi.StationApi;

@RunWith(Parameterized.class)
public class testRentBike {
	private float cost;
	private float balance;
	private Boolean res;
	
	public testRentBike(float cost, float balance, Boolean res) {
		super();
		this.cost = cost;
		this.balance = balance;
		this.res = res;
	}
	
	@Parameterized.Parameters
	public static Collection<Object[]> Statics() {
		return Arrays.asList(new Object[][] { 
			{1, 3000, true },
			{ 5000, 300, false}, 

		});
	}
	
	@Test
	public void testCreateStation() {	
		RentAPI  rent = new RentAPI();
		Boolean result = rent.checkRentBike(cost, balance);
		System.out.println("result " + result + " " + res);
		assertEquals(result,res);
	}
}
