package com.ebr.components.generalbike.controller.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ebr.bean.TwinBike;
import com.ebr.components.generalbike.twinbike.controller.AdminTwinBikePageController;
import com.ebr.serverapi.GeneralBikeApi;

@RunWith(Parameterized.class)
public class ParameterizedUpdateTwinBike {
	private String name;
	private String weight;
	private String licensePlate;
	private String producer;
	private String cost;

	private String expectedName;
	private String expectedWeight;
	private String expectedLicensePlate;
	private String expectedProducer;
	private String expectedCost;

	private AdminTwinBikePageController controller = new AdminTwinBikePageController();
	private GeneralBikeApi api = new GeneralBikeApi();
	
	public ParameterizedUpdateTwinBike(String name, String weight, String licensePlate, String producer, String cost,
			String expectedName, String expectedWeight, String expectedLicensePlate, String expectedProducer,
			String expectedCost) {
		super();
		this.name = name;
		this.weight = weight;
		this.licensePlate = licensePlate;
		this.producer = producer;
		this.cost = cost;
		this.expectedName = expectedName;
		this.expectedWeight = expectedWeight;
		this.expectedLicensePlate = expectedLicensePlate;
		this.expectedProducer = expectedProducer;
		this.expectedCost = expectedCost;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> input() {
		return Arrays.asList(new Object[][] { 
			{ "New Name 1", "15.5", "New License Plate 1", "New Producer 1", "199000.9",
				"New Name 1", "15.5", "New License Plate 1", "New Producer 1", "199000.9"},
			{ "New Name 2", "25.5", "New License Plate 2", "New Producer 2", "299000.9",
					"New Name 2", "25.5", "New License Plate 2", "New Producer 2", "299000.9"},
			{ "New Name 3", "35.5", "New License Plate 3", "New Producer 3", "399000.9",
						"New Name 3", "35.5", "New License Plate 3", "New Producer 3", "399000.9"}
		});
	}

	@Test
	public void testUpdateBike() {
		ArrayList<TwinBike> list = api.getTwinBikes(null);
		assertTrue("No data", list.size() > 0);

		TwinBike twinBike = list.get(0);
		twinBike.setName(name);
		twinBike.setWeight(Float.valueOf(weight));
		twinBike.setLicensePlate(licensePlate);
		twinBike.setProducer(producer);
		twinBike.setCost(Float.valueOf(cost));
		
		controller.updateGeneralBike(twinBike);

		HashMap<String, String> params = new HashMap<String, String>();
		params.put("id", twinBike.getId());
		list = api.getTwinBikes(params);
		assertTrue("Error in updateBike API!", list.size() > 0);

		TwinBike newTwinBike = api.getTwinBikes(params).get(0);
		assertEquals("Error in updateBike field name!", newTwinBike.getName(), expectedName);
		assertEquals("Error in updateBike field weight!", String.valueOf(newTwinBike.getWeight()), expectedWeight);
		assertEquals("Error in updateBike field licensePlate!", newTwinBike.getLicensePlate(), expectedLicensePlate);
		assertEquals("Error in updateBike field producer!", newTwinBike.getProducer(), expectedProducer);
		assertEquals("Error in updateBike field cost!", String.valueOf(newTwinBike.getCost()), expectedCost);
	}
}
