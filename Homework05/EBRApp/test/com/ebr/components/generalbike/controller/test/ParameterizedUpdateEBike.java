package com.ebr.components.generalbike.controller.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ebr.bean.EBike;
import com.ebr.components.generalbike.ebike.controller.AdminEBikePageController;
import com.ebr.serverapi.GeneralBikeApi;

@RunWith(Parameterized.class)
public class ParameterizedUpdateEBike {
	private String name;
	private String weight;
	private String licensePlate;
	private String producer;
	private String cost;
	private String batteryPercentage;
	private String loadCycles;
	private String estimatedUsageTimeRemaining;

	private String expectedName;
	private String expectedWeight;
	private String expectedLicensePlate;
	private String expectedProducer;
	private String expectedCost;
	private String expectedBatteryPercentage;
	private String expectedLoadCycles;
	private String expectedEstimatedUsageTimeRemaining;

	private AdminEBikePageController controller = new AdminEBikePageController();
	private GeneralBikeApi api = new GeneralBikeApi();

	public ParameterizedUpdateEBike(String name, String weight, String licensePlate, String producer, String cost,
			String batteryPercentage, String loadCycles, String estimatedUsageTimeRemaining, String expectedName,
			String expectedWeight, String expectedLicensePlate, String expectedProducer, String expectedCost,
			String expectedBatteryPercentage, String expectedLoadCycles, String expectedEstimatedUsageTimeRemaining) {
		super();
		this.name = name;
		this.weight = weight;
		this.licensePlate = licensePlate;
		this.producer = producer;
		this.cost = cost;
		this.batteryPercentage = batteryPercentage;
		this.loadCycles = loadCycles;
		this.estimatedUsageTimeRemaining = estimatedUsageTimeRemaining;
		this.expectedName = expectedName;
		this.expectedWeight = expectedWeight;
		this.expectedLicensePlate = expectedLicensePlate;
		this.expectedProducer = expectedProducer;
		this.expectedCost = expectedCost;
		this.expectedBatteryPercentage = expectedBatteryPercentage;
		this.expectedLoadCycles = expectedLoadCycles;
		this.expectedEstimatedUsageTimeRemaining = expectedEstimatedUsageTimeRemaining;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> input() {
		return Arrays.asList(new Object[][] { 
			{ "New Name 1", "15.5", "New License Plate 1", "New Producer 1", "199000.9", "90.5", "1", "2",
				"New Name 1", "15.5", "New License Plate 1", "New Producer 1", "199000.9", "90.5", "1", "2"},
			{ "New Name 2", "25.5", "New License Plate 2", "New Producer 2", "299000.9", "80.5", "3", "4",
					"New Name 2", "25.5", "New License Plate 2", "New Producer 2", "299000.9", "80.5", "3", "4"},
			{ "New Name 3", "35.5", "New License Plate 3", "New Producer 3", "399000.9", "70.5", "5", "6",
						"New Name 3", "35.5", "New License Plate 3", "New Producer 3", "399000.9", "70.5", "5", "6"}
		});
	}

	@Test
	public void testUpdateBike() {
		ArrayList<EBike> list = api.getEBikes(null);
		assertTrue("No data", list.size() > 0);

		EBike eBike = list.get(0);
		eBike.setName(name);
		eBike.setWeight(Float.valueOf(weight));
		eBike.setLicensePlate(licensePlate);
		eBike.setProducer(producer);
		eBike.setCost(Float.valueOf(cost));
		eBike.setBatteryPercentage(Float.valueOf(batteryPercentage));
		eBike.setLoadCycles(Integer.valueOf(loadCycles));
		eBike.setEstimatedUsageTimeRemaining(Integer.valueOf(estimatedUsageTimeRemaining));

		controller.updateGeneralBike(eBike);

		HashMap<String, String> params = new HashMap<String, String>();
		params.put("id", eBike.getId());
		list = api.getEBikes(params);
		assertTrue("Error in updateBike API!", list.size() > 0);

		EBike newEBike = api.getEBikes(params).get(0);
		assertEquals("Error in updateBike field name!", newEBike.getName(), expectedName);
		assertEquals("Error in updateBike field weight!", String.valueOf(newEBike.getWeight()), expectedWeight);
		assertEquals("Error in updateBike field licensePlate!", newEBike.getLicensePlate(), expectedLicensePlate);
		assertEquals("Error in updateBike field producer!", newEBike.getProducer(), expectedProducer);
		assertEquals("Error in updateBike field cost!", String.valueOf(newEBike.getCost()), expectedCost);
		assertEquals("Error in updateBike field batteryPercentage!", String.valueOf(newEBike.getBatteryPercentage()), expectedBatteryPercentage);
		assertEquals("Error in updateBike field loadCycles!", String.valueOf(newEBike.getLoadCycles()), expectedLoadCycles);
		assertEquals("Error in updateBike field estimatedUsageTimeRemaining!", String.valueOf(newEBike.getEstimatedUsageTimeRemaining()), expectedEstimatedUsageTimeRemaining);
	}
}
