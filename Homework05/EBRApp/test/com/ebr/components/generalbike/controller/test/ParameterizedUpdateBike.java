package com.ebr.components.generalbike.controller.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ebr.bean.Bike;
import com.ebr.components.generalbike.bike.controller.AdminBikePageController;
import com.ebr.serverapi.GeneralBikeApi;

@RunWith(Parameterized.class)
public class ParameterizedUpdateBike {
	private String name;
	private String weight;
	private String licensePlate;
	private String producer;
	private String cost;

	private String expectedName;
	private String expectedWeight;
	private String expectedLicensePlate;
	private String expectedProducer;
	private String expectedCost;

	private AdminBikePageController controller = new AdminBikePageController();
	private GeneralBikeApi api = new GeneralBikeApi();
	
	public ParameterizedUpdateBike(String name, String weight, String licensePlate, String producer, String cost,
			String expectedName, String expectedWeight, String expectedLicensePlate, String expectedProducer,
			String expectedCost) {
		super();
		this.name = name;
		this.weight = weight;
		this.licensePlate = licensePlate;
		this.producer = producer;
		this.cost = cost;
		this.expectedName = expectedName;
		this.expectedWeight = expectedWeight;
		this.expectedLicensePlate = expectedLicensePlate;
		this.expectedProducer = expectedProducer;
		this.expectedCost = expectedCost;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> input() {
		return Arrays.asList(new Object[][] { 
			{ "New Name 1", "15.5", "New License Plate 1", "New Producer 1", "199000.9",
				"New Name 1", "15.5", "New License Plate 1", "New Producer 1", "199000.9"},
			{ "New Name 2", "25.5", "New License Plate 2", "New Producer 2", "299000.9",
					"New Name 2", "25.5", "New License Plate 2", "New Producer 2", "299000.9"},
			{ "New Name 3", "35.5", "New License Plate 3", "New Producer 3", "399000.9",
						"New Name 3", "35.5", "New License Plate 3", "New Producer 3", "399000.9"}
		});
	}

	@Test
	public void testUpdateBike() {
		ArrayList<Bike> list = api.getBikes(null);
		assertTrue("No data", list.size() > 0);

		Bike bike = list.get(0);
		bike.setName(name);
		bike.setWeight(Float.valueOf(weight));
		bike.setLicensePlate(licensePlate);
		bike.setProducer(producer);
		bike.setCost(Float.valueOf(cost));
		
		controller.updateGeneralBike(bike);

		HashMap<String, String> params = new HashMap<String, String>();
		params.put("id", bike.getId());
		list = api.getBikes(params);
		assertTrue("Error in updateBike API!", list.size() > 0);

		Bike newBike = api.getBikes(params).get(0);
		assertEquals("Error in updateBike field name!", newBike.getName(), expectedName);
		assertEquals("Error in updateBike field weight!", String.valueOf(newBike.getWeight()), expectedWeight);
		assertEquals("Error in updateBike field licensePlate!", newBike.getLicensePlate(), expectedLicensePlate);
		assertEquals("Error in updateBike field producer!", newBike.getProducer(), expectedProducer);
		assertEquals("Error in updateBike field cost!", String.valueOf(newBike.getCost()), expectedCost);
	}
}
