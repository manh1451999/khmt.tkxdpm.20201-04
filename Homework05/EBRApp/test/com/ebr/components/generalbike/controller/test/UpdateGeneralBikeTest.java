package com.ebr.components.generalbike.controller.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ParameterizedUpdateBike.class, ParameterizedUpdateEBike.class,
		ParameterizedUpdateTwinBike.class })
public class UpdateGeneralBikeTest {

}
