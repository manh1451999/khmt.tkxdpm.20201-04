package com.ebr.components.station.controller.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ParameterizedUpdateStation.class })
public class UpdateStationTest {

}
