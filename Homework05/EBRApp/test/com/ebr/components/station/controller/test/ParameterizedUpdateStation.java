package com.ebr.components.station.controller.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ebr.bean.Station;
import com.ebr.components.station.controller.AdminStationPageController;
import com.ebr.serverapi.StationApi;

@RunWith(Parameterized.class)
public class ParameterizedUpdateStation {
	private String name;
	private String address;
	private int numberOfEmptyDocks;

	private String expectedName;
	private String expectedAddress;
	private int expectedNumberOfEmptyDocks;

	private AdminStationPageController controller = new AdminStationPageController();
	private StationApi api = new StationApi();
	
//	public ParameterizedUpdateStation(String name, String address, int numberOfEmptyDocks, 
//			String expectedName, String expectedAddress, int expectedNumberOfEmptyDocks ) {
	public ParameterizedUpdateStation(String name, String expectedName ) {
		super();
		this.name = name;
//		this.address = address;
//		this.numberOfEmptyDocks = numberOfEmptyDocks;
		
		this.expectedName = expectedName;
//		this.expectedAddress = expectedAddress;
//		this.expectedNumberOfEmptyDocks = expectedNumberOfEmptyDocks;
		
	}

	@Parameterized.Parameters
	public static Collection<Object[]> input() {
		return Arrays.asList(new Object[][] { 
			{ "Bai xe Phuong Mai", "New Address 1"},
			{ "Bai xe Truong Dinh", "TD"},
			{ "Bai xe Tan Mai", "TM"}
		});
	}

	@Test
	public void testUpdateStation() {
		ArrayList<Station> list = api.getStations(null);
		assertTrue("No data", list.size() > 0);

		Station station = list.get(0);
		station.setName(name);
//		station.setAddress(address);
//		station.setNumberOfEmptyDocks(Integer.valueOf(numberOfEmptyDocks));
		
		controller.updateStation(station);

		HashMap<String, String> params = new HashMap<String, String>();
		params.put("id", station.getId());
		list = api.getStations(params);
		assertTrue("Error in updateStation API!", list.size() > 0);

		Station newStation = api.getStations(params).get(0);
		System.out.println(newStation.getName()+ expectedName);
		assertEquals("Error in updateStation field name!", newStation.getName(), expectedName);
//		assertEquals("Error in updateStation field address!", String.valueOf(newStation.getAddress()), expectedAddress);
//		assertEquals("Error in updateStation field number of empty docks!", newStation.getNumberOfEmptyDocks(), expectedNumberOfEmptyDocks);
		
	}
}
