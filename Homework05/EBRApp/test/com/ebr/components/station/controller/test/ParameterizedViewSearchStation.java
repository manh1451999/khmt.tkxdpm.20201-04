package com.ebr.components.station.controller.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ebr.bean.Station;
import com.ebr.serverapi.StationApi;




@RunWith(Parameterized.class)
public class ParameterizedViewSearchStation {
	private String nameTitle;
	private String expectedResult;


	private StationApi stationApi = new StationApi();
	
	public ParameterizedViewSearchStation(String nameTitle, String expectedResult) {
		super();
		this.nameTitle= nameTitle;
		this.expectedResult= expectedResult;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> input() {
		return Arrays.asList(new Object[][] { 
			{"Thuong Kiet", "Bai xe Ly Thuong Kiet"},
			{"Phuong Mai", "Bai xe Phuong Mai"},
			{"Truong Dinh", "Bai xe Truong Dinh"},
			
		});
	}

	@Test
	public void testUpdateBike() {
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("name", nameTitle);
		
		ArrayList<Station> list= stationApi.getStations(params);
		assertTrue("No data", list.size() > 0);
		
		System.out.println(list.size());
		
		Station station = list.get(0);
		
//		System.out.println(station.si);
		
		assertEquals("Eror in getStation API!", station.getName(), expectedResult);
	}
}
