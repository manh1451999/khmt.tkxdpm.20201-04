package com.ebr.components.station.controller.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({ ParameterizedViewBike.class, ParameterizedViewSearchStation.class })
public class ParameterizedTestSearchStation {

}
