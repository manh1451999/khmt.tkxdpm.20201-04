package com.ebr.components.station.controller.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import com.ebr.bean.Bike;
import com.ebr.bean.Station;
import com.ebr.serverapi.GeneralBikeApi;
import com.ebr.serverapi.StationApi;




@RunWith(Parameterized.class)
public class ParameterizedViewBike {
	private String idStation;
	private int countBike;
	private String idBikesResult;
	private String idBikes;
	

	private GeneralBikeApi bikeApi = new GeneralBikeApi();
	private StationApi stationApi = new StationApi();
	
	public ParameterizedViewBike(String idStation, int countBike, String idBikesResult) {
		super();
		this.idStation= idStation;
		this.countBike= countBike;
		this.idBikesResult= idBikesResult;
	}

	@Parameterized.Parameters
	public static Collection<Object[]> input() {
		return Arrays.asList(new Object[][] { 
			{"station1", 3,"normalbike1|normalbike2|normalbike3|" },
			{"station3", 2,"normalbike2|normalbike3|" }
			
		});
	}

	@Test
	public void testUpdateBike() {
		
		
		HashMap<String, String> params = new HashMap<String, String>();
		params.put("id", idStation);
		
		ArrayList<Station> list= stationApi.getStations(params);
		assertTrue("No data", list.size() > 0);

		Station station = list.get(0);
		ArrayList<String> listID= station.getListBikeId();
		
		List<Bike> listBike = new ArrayList<Bike>();
		
		idBikes="";
		for(String id: listID) {
			Map<String, String> param = new HashMap<String, String>();
			param.put("id", id);
			if(bikeApi.getBikes(param).size()>0) {
				listBike.addAll(bikeApi.getBikes(param));	
			}
		}
		
		for(Bike bike: listBike) {
			idBikes += bike.getId()+"|";
		}
		
		System.out.println(listBike.size());
		System.out.println(idBikes);
		assertEquals("khong du so luong bike", listBike.size(), countBike);
		assertEquals("sai cac xe trong bai", idBikes, idBikesResult);
	}
}
