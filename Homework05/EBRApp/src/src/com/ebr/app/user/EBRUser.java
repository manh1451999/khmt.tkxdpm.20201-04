package com.ebr.app.user;

import java.awt.BorderLayout;

import javax.swing.*;

@SuppressWarnings("serial")
public class EBRUser extends JFrame {

	public static final int WINDOW_WIDTH = 800;
	public static final int WINDOW_HEIGHT = 550;
	
	public EBRUser(EBRUserController controller) {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);
		
		
//		rootPanel.add(controller.getCartPane(), BorderLayout.NORTH);
		
		
		JTabbedPane tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);
		
		
		JPanel stationPage = controller.getStationPage();
		tabbedPane.addTab("Station", null, stationPage, "Stations");
		

		JPanel stationReturnPage = controller.getReturnStationPage();
		tabbedPane.addTab("Tra xe", null, stationReturnPage, "Tra xe");
		
//		tabbedPane.addTab("Compact Discs", null, new JPanel(), "Compact Discs");
//		tabbedPane.addTab("Digital Video Discs", null, new JPanel(), "Digital Video Discs");



		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Online Media System for User");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

	public static void main(String[] args) {
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				new EBRUser(new EBRUserController());
			}
		});
	}
}