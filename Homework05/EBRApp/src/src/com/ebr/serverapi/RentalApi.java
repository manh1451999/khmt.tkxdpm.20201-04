package com.ebr.serverapi;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.bean.Rental;
import com.ebr.bean.Station;

public class RentalApi {
public static final String PATH = "http://localhost:8080/";
	
	private Client client;
	
	public RentalApi() {
		client = ClientBuilder.newClient();
	}
	
	public ArrayList<Rental> getAllRental() {
		WebTarget webTarget = client.target(PATH).path("rentals");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<Rental> res = response.readEntity(new GenericType<ArrayList<Rental>>(){});
		System.out.println(res);
		return res;
	}
	
	public ArrayList<Rental> getRental(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("rentals");
		
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Rental> res = response.readEntity(new GenericType<ArrayList<Rental>>() {});
		System.out.println(res);
		return res;
	}
	
}
