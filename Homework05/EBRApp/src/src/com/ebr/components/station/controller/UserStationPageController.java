package com.ebr.components.station.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.ebr.bean.Station;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.abstractdata.gui.ADataListPane;
import com.ebr.components.stationUser.gui.StationSearchPane;
import com.ebr.components.stationUser.gui.StationSinglePane;
import com.ebr.components.stationUser.gui.UserStationListPane;
import com.ebr.serverapi.StationApi;

public class UserStationPageController extends ADataPageController<Station>{
	public UserStationPageController() {
		super();
	}
//	public UserStationPageController(CartController cartController) {
//		super(cartController);
//	}
	@Override
	public List<? extends Station> search(Map<String, String> searchParams) {
		System.out.println(searchParams);
		System.out.println("Tim Kiem");
//		System.out.println(new StationApi().getAllStations());
//		return new StationApi().getAllStations();
		return new StationApi().getStations(searchParams);
	}
	@Override
	public StationSinglePane createSinglePane() {
		return new StationSinglePane();
	}
	@Override
	public StationSearchPane createSearchPane() {
		return new StationSearchPane();
	}
	
	@Override
	public ADataListPane<Station> createListPane() {
		return new UserStationListPane(this);
	}
}
