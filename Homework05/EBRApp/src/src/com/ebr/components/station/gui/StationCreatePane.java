package com.ebr.components.station.gui;

import java.util.HashMap;
import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ebr.bean.Station;
import com.ebr.components.abstractdatastation.controller.IDataStationManageController;
import com.ebr.components.abstractdatastation.gui.ADataCreateDialog;

public class StationCreatePane<T> extends ADataCreateDialog<Station> {
	
	private JTextField nameField;
	private JTextField addressField;
	private JTextField emptyDockField;
	
	public StationCreatePane(IDataStationManageController<Station> controller) {
		super(controller);
		// TODO Auto-generated constructor stub
	}

	public void buildControls() {
		int row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Name");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);
		
		
		row = getLastRowIndex();
		JLabel addressLabel = new JLabel("Address");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(addressLabel, c);
		addressField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(addressField, c);
		
		
		row = getLastRowIndex();
		JLabel emptyDockLabel = new JLabel("Number of empty docks");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(emptyDockLabel, c);
		emptyDockField = new JTextField(15);
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(emptyDockField, c);
	}

	@Override
	public Station getNewData() {
		t.setName(nameField.getText());
		t.setAddress(addressField.getText());
		t.setNumberOfEmptyDocks(Integer.parseInt(emptyDockField.getText()));
		System.out.println(t);
		return t;
	}

	@Override
	public Map<String, String> getDataCreate() {
		Map<String, String> map = new HashMap<String, String>();
		 map.put("name",nameField.getText());
		 map.put("address", addressField.getText());
		 map.put("numberOfEmptyDocks", emptyDockField.getText());
		return map;
	}

	

}
