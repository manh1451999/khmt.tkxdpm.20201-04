package com.ebr.components.station.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ebr.bean.Station;
import com.ebr.components.abstractdatastation.controller.IDataStationManageController;
import com.ebr.components.abstractdatastation.gui.ADataStationEditDialog;

@SuppressWarnings("serial")
public class StationEditDialog extends ADataStationEditDialog<Station> {
	private JTextField nameField;
	private JTextField addressField;
	private JTextField numOfEmptyDocksField;
	private JTextField producerField;
	private JTextField costField;

	public StationEditDialog(Station station, IDataStationManageController<Station> controller) {
		super(station, controller);
	}

	@Override
	public void buildControls() {
		int row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Name");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);
		nameField.setText(t.getName());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);

		row = getLastRowIndex();
		JLabel addressLabel = new JLabel("Address");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(addressLabel, c);
		addressField = new JTextField(15);
		addressField.setText(t.getAddress());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(addressField, c);

		row = getLastRowIndex();
		JLabel numOfEmptyDocksLabel = new JLabel("Empty Docks");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(numOfEmptyDocksLabel, c);
		numOfEmptyDocksField = new JTextField(15);
		numOfEmptyDocksField.setText(t.getNumberOfEmptyDocks()+ "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(numOfEmptyDocksField, c);

		
	}

	@Override
	public Station getNewData() {
		t.setName(nameField.getText());
		t.setAddress(addressField.getText());
		t.setNumberOfEmptyDocks(Integer.valueOf(numOfEmptyDocksField.getText()));

		return t;
	}
}
