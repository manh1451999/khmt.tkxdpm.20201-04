package com.ebr.components.abstractdatageneralbike.gui;

import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;

@SuppressWarnings("serial")
public abstract class ADataGeneralBikeSinglePane<T> extends JPanel {
	protected T t;

	protected GridBagLayout layout;
	protected GridBagConstraints c;

	private JPanel panel;

	public ADataGeneralBikeSinglePane() {
		buildControls();
	}

	public ADataGeneralBikeSinglePane(T t) {
		this();
		this.t = t;

		displayDataGeneralBike();
	}

	public void buildControls() {
		layout = new GridBagLayout();
		this.setLayout(layout);
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1;
	}

	public abstract void displayDataGeneralBike();

	public void addDataHandlingComponent(Component component) {
		if (panel == null) {
			int row = getLastRowIndex();
			c.gridx = 0;
			c.gridy = row;
			panel = new JPanel();
			this.add(panel, c);
			panel.setLayout(new FlowLayout(FlowLayout.LEFT));
		}

		panel.add(component);
	}

	public void updateDataGeneralBike(T t) {
		this.t = t;
		displayDataGeneralBike();
	}

	public T getDataGeneralBike() {
		return this.t;
	}

	protected int getLastRowIndex() {
		layout.layoutContainer(this);
		int[][] dim = layout.getLayoutDimensions();
		int rows = dim[1].length;
		return rows;
	}
}
