package com.ebr.components.abstractdatageneralbike.gui;

import javax.swing.JPanel;
import javax.swing.SpringLayout;

@SuppressWarnings("serial")
public class ADataGeneralBikePagePane<T> extends JPanel {
	public ADataGeneralBikePagePane(ADataGeneralBikeSearchPane generalBikeSearchPane,
			ADataGeneralBikeListPane<T> generalBikeListPane) {
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);

		this.add(generalBikeSearchPane);
		this.add(generalBikeListPane);

		layout.putConstraint(SpringLayout.WEST, generalBikeSearchPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, generalBikeSearchPane, 5, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.EAST, generalBikeSearchPane, -5, SpringLayout.EAST, this);

		layout.putConstraint(SpringLayout.WEST, generalBikeListPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, generalBikeListPane, 5, SpringLayout.SOUTH, generalBikeSearchPane);
		layout.putConstraint(SpringLayout.EAST, generalBikeListPane, -5, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, generalBikeListPane, -5, SpringLayout.SOUTH, this);
	}
}
