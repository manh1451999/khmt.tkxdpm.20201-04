package com.ebr.components.abstractdatastation.controller;
import java.util.Map;

public interface IDataStationSearchController {
	public void search(Map<String, String> searchParams);

}
