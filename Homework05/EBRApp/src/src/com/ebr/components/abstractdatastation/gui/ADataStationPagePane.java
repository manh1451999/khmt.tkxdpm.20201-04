package com.ebr.components.abstractdatastation.gui;

import javax.swing.JPanel;
import javax.swing.SpringLayout;

@SuppressWarnings("serial")
public class ADataStationPagePane<T> extends JPanel {
	public ADataStationPagePane(ADataStationSearchPane stationSearchPane,
			ADataStationListPane<T> stationListPane) {
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);

		this.add(stationSearchPane);
		this.add(stationListPane);

		layout.putConstraint(SpringLayout.WEST, stationSearchPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, stationSearchPane, 5, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.EAST, stationSearchPane, -5, SpringLayout.EAST, this);

		layout.putConstraint(SpringLayout.WEST, stationListPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, stationListPane, 5, SpringLayout.SOUTH, stationSearchPane);
		layout.putConstraint(SpringLayout.EAST, stationListPane, -5, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, stationListPane, -5, SpringLayout.SOUTH, this);
	}
}
