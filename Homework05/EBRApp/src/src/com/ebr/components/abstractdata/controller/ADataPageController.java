package com.ebr.components.abstractdata.controller;

import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import com.ebr.components.abstractdata.gui.ADataListPane;
import com.ebr.components.abstractdata.gui.ADataPagePane;
import com.ebr.components.abstractdata.gui.ADataSearchPane;
import com.ebr.components.abstractdata.gui.ADataSinglePane;

public abstract class ADataPageController<T> {
	protected ADataPagePane<T> pagePane;
	protected ADataPagePane<T> pageReturnPane;
	
	protected ADataListPane<T> listReturn;
	protected ADataListPane<T> listPane;
	public ADataPageController() {
		ADataSearchPane searchPane = createSearchPane();
		ADataSearchPane searchReturnPane = createSearchPane();
		
		 listPane = createListPane();
		 listReturn = createListPane();
		
		searchPane.setController(new IDataSearchController() {
			@Override
			public void search(Map<String, String> searchParams) {
				List<? extends T> list = ADataPageController.this.search(searchParams);
				listPane.updateData(list);
				listReturn.updateReturnData(list);
			}
		});
		

		searchReturnPane.setController(new IDataSearchController() {
			@Override
			public void search(Map<String, String> searchParams) {
				List<? extends T> list = ADataPageController.this.search(searchParams);
				listPane.updateData(list);
				listReturn.updateReturnData(list);
			}
		});
		
		searchPane.fireSearchEvent();
		
		pageReturnPane = new ADataPagePane<T>(searchReturnPane, listReturn);
		pagePane = new ADataPagePane<T>(searchPane, listPane);
	}
	
	public JPanel getDataPagePane() {
		return pagePane;
	}
	

	public JPanel getReturnDataPagePane() {
		return pageReturnPane;
	}
	
	
	public ADataPageController(List<? extends T> list) {
		ADataSearchPane searchPane = createSearchPane();
		
//		ADataListPane<T> listPane = createListPane();
		
//		searchPane.setController(new IDataSearchController() {
//			@Override
//			public void search(Map<String, String> searchParams) {
//				Map<String, String> map = new HashMap<String, String>();
//		        map.put("name", "A");
//		 
//				List<? extends T> list = ADataPageController.this.search(map);
//				listPane.updateData(list);
//			}
//		});
//		
//		searchPane.fireSearchEvent();
		
//		pagePane = new ADataPagePane<T>(searchPane, listPane);
	}
	
	
	
	public abstract ADataSearchPane createSearchPane();

	public abstract List<? extends T> search(Map<String, String> searchParams);
	
	
	
	public abstract ADataSinglePane<T> createSinglePane();
	
	public abstract ADataListPane<T> createListPane();
}
