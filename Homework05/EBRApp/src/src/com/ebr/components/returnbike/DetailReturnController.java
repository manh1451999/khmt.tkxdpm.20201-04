package com.ebr.components.returnbike;

import java.util.List;

import javax.swing.JPanel;

import com.ebr.bean.Bike;
import com.ebr.bean.Rental;
import com.ebr.bean.SingleBike;



public class DetailReturnController {
	
	public JPanel getSingleReturnPage(List<Rental> list) {
		SingleReturnPageController controller = new SingleReturnPageController(list);
		return controller.getDataPagePane();
	}
}
