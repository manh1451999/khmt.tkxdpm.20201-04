package com.ebr.components.returnbike;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import com.ebr.bean.Bike;
import com.ebr.bean.Rental;
import com.ebr.bean.SingleBike;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.abstractdata.gui.ADataListPane;
import com.ebr.components.abstractdata.gui.ADataPagePane;
import com.ebr.components.abstractdata.gui.ADataSearchPane;
import com.ebr.components.abstractdata.gui.ADataSinglePane;


public class SingleReturnPageController extends ADataPageController<Rental> {
	protected ReturnBikeThisStation<Rental> pagePane;
	protected ADataListPane<Rental> listPane;
	public SingleReturnPageController(List<? extends Rental> list) {
		super(list);
		listPane = createListPane(list);
		pagePane = new ReturnBikeThisStation<Rental>(listPane);
		// TODO Auto-generated constructor stub
	}

//	private CartController cartController;

	@Override
	public List<? extends Rental> search(Map<String, String> searchParams) {
		List<Rental> list = new ArrayList<Rental>();
		return list;
	}

	@Override
	public ADataSinglePane<Rental> createSinglePane() {
		// TODO Auto-generated method stub
		return new ReturnSinglePane();
	}
	
	public ADataListPane<Rental> createListPane(List<? extends Rental> list) {
		// TODO Auto-generated method stub
		RentalBikeListPane singleReturnListPane = new RentalBikeListPane(this);
		singleReturnListPane.displayData(list);
		return singleReturnListPane;
	}

	@Override
	public ADataSearchPane createSearchPane() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ADataListPane<Rental> createListPane() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public JPanel getDataPagePane() {
		return pagePane;
	}
	
	public JPanel getReturnDataPagePane() {
		return pageReturnPane;
	}
	
}
