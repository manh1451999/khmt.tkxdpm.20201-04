package com.ebr.components.returnbike;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.ebr.bean.Bike;
import com.ebr.bean.Rental;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.abstractdata.gui.ADataListPane;
import com.ebr.components.abstractdata.gui.ADataSinglePane;


public class RentalBikeListPane extends ADataListPane<Rental>{
	
	public RentalBikeListPane(ADataPageController<Rental> controller) {
		this.controller = controller;
	}
	
	
	@Override
	public void decorateSingleReturnPane(ADataSinglePane<Rental> singlePane) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void decorateSinglePane(final ADataSinglePane<Rental> singlePane) {
		
		JButton button = new JButton("Tra xe nay");
		singlePane.addDataHandlingComponent(button);
		
		button.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				Rental rental = singlePane.getData();
				System.out.println(singlePane.getData().getThoiGianBatDauThue());
				
				DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss dd-MM-yyyy");
		        // get end time and set for rental
		        LocalDateTime endAt =  LocalDateTime.now();
		        //get start time
		        LocalDateTime startAt = LocalDateTime.parse(rental.getThoiGianBatDauThue(), formatter); 
		            
		        Duration duration = Duration.between(startAt, endAt);
		        float longRent = (float)(duration.toMinutes());
				float tienTra = 0;
				if(longRent <= 10) {
					tienTra = 0;
				} else if(longRent <= 30){
		            tienTra = 10000;
		        } else {
		        	tienTra = (float)(10000 + 3000 * Math.ceil((longRent - 30)/15));
		        }
				
		        if(rental.getLoaiXe() != "Bike") {
		        	tienTra = (float)(tienTra * 1.5);
		        }
				JOptionPane.showMessageDialog(new JFrame(), "Ban da thue xe voi thoi gian: " + longRent + "(phut).\nBan da coc truoc do "+rental.getTienCoc() + "\nSo tien can phai tra la: " + tienTra, "Thong bao thanh toan", JOptionPane.INFORMATION_MESSAGE );
//				rentalBikeController.rentalSingleBikeDialog(bike);
			}
		});
	}
}
