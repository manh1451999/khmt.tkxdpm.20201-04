package com.ebr.components.stationUser.gui;



import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ebr.components.abstractdata.gui.ADataSearchPane;



public class StationSearchPane extends ADataSearchPane {
	private JTextField nameField;
	private JTextField addressField;
	private JTextField numberOfEmptyDocksField;
	private JTextField timeField;
	

	public StationSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		JLabel nameLabel = new JLabel("Name");
		nameField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(nameLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(nameField, c);
		
		
		JLabel addressLabel = new JLabel("Address");
		addressField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(addressLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(addressField, c);
		
		
		JLabel maxSlotLabel = new JLabel("Number Of Empty Docks");
		numberOfEmptyDocksField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(maxSlotLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(numberOfEmptyDocksField, c);
		
		
		JLabel currentSlotLabel = new JLabel("Time");
		timeField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(currentSlotLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(timeField, c);
	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		if (!nameField.getText().trim().equals("")) {
			res.put("name", nameField.getText().trim());
		}
		if (!addressField.getText().trim().equals("")) {
			res.put("address", addressField.getText().trim());
		}
		
		if (!numberOfEmptyDocksField.getText().trim().equals("")) {
			res.put("numberOfEmptyDocks", numberOfEmptyDocksField.getText().trim());
		}
		
		if (!timeField.getText().trim().equals("")) {
			res.put("time", timeField.getText().trim());
		}
		
		return res;
	}
}
