package com.ebr.components.stationUser.gui;

import javax.swing.JLabel;

import com.ebr.bean.Station;
import com.ebr.components.abstractdata.gui.ADataSinglePane;



@SuppressWarnings("serial")
public class StationSinglePane  extends ADataSinglePane<Station>{
	private JLabel labelName;
	private JLabel labelAddress;
	private JLabel labelNumberOfEmptyDocks;
	private JLabel labelTime;
	private JLabel labelDistance;
	
	@Override
	public void buildControls() {
		// TODO Auto-generated method stub
		super.buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelName = new JLabel();
		add(labelName, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelAddress = new JLabel();
		add(labelAddress, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelNumberOfEmptyDocks = new JLabel();
		add(labelNumberOfEmptyDocks, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelTime = new JLabel();
		add(labelTime, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelDistance = new JLabel();
		add(labelDistance, c);
	}
	
	@Override
	public void displayData() {
		// TODO Auto-generated method stub
		labelName.setText("Name: " + t.getName());
		labelAddress.setText("Address: " + t.getAddress());
		labelNumberOfEmptyDocks.setText("Number Of EmptyDocks: " + t.getNumberOfEmptyDocks()+ "");
		labelTime.setText("Time: " + t.getTime());
		labelDistance.setText("Distance: " + t.getDistance());
	}

}
