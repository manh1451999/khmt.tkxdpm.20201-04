package com.ebr.components.generalbike.twinbike.gui;

import com.ebr.bean.GeneralBike;
import com.ebr.components.abstractdatageneralbike.controller.IDataGeneralBikeManageController;
import com.ebr.components.generalbike.gui.GeneralBikeEditDialog;

@SuppressWarnings("serial")
public class TwinBikeEditDialog extends GeneralBikeEditDialog {
	public TwinBikeEditDialog(GeneralBike generalBike, IDataGeneralBikeManageController<GeneralBike> controller) {
		super(generalBike, controller);
	}

	@Override
	public void buildControls() {
		super.buildControls();
	}

	@Override
	public GeneralBike getNewData() {
		super.getNewData();
		
		return t;
	}
}
