package com.ebr.components.generalbike.bike.gui;

import com.ebr.bean.GeneralBike;
import com.ebr.components.abstractdatageneralbike.controller.IDataGeneralBikeManageController;
import com.ebr.components.generalbike.gui.GeneralBikeEditDialog;

@SuppressWarnings("serial")
public class BikeEditDialog extends GeneralBikeEditDialog {
	public BikeEditDialog(GeneralBike generalBike, IDataGeneralBikeManageController<GeneralBike> controller) {
		super(generalBike, controller);
	}

	@Override
	public void buildControls() {
		super.buildControls();
	}

	@Override
	public GeneralBike getNewData() {
		super.getNewData();
		
		return t;
	}
}
