package com.ebr.components.generalbike.bike.gui;

import java.util.Map;

import com.ebr.components.generalbike.gui.GeneralBikeSearchPane;

@SuppressWarnings("serial")
public class BikeSearchPane extends GeneralBikeSearchPane {
	public BikeSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		super.buildControls();
	}
	
	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		return res;
	}
}
