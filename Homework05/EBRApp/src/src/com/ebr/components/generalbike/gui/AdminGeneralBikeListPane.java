package com.ebr.components.generalbike.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

import com.ebr.bean.Bike;
import com.ebr.bean.EBike;
import com.ebr.bean.GeneralBike;
import com.ebr.bean.TwinBike;
import com.ebr.components.abstractdatageneralbike.controller.ADataGeneralBikePageController;
import com.ebr.components.abstractdatageneralbike.controller.IDataGeneralBikeManageController;
import com.ebr.components.abstractdatageneralbike.gui.ADataGeneralBikeListPane;
import com.ebr.components.abstractdatageneralbike.gui.ADataGeneralBikeSinglePane;
import com.ebr.components.generalbike.bike.gui.BikeEditDialog;
import com.ebr.components.generalbike.controller.AdminGeneralBikePageController;
import com.ebr.components.generalbike.ebike.gui.EBikeEditDialog;
import com.ebr.components.generalbike.twinbike.gui.TwinBikeEditDialog;

@SuppressWarnings("serial")
public class AdminGeneralBikeListPane extends ADataGeneralBikeListPane<GeneralBike> {
	public AdminGeneralBikeListPane(ADataGeneralBikePageController<GeneralBike> controller) {
		this.controller = controller;
	}

	@Override
	public void decorateGeneralBikeSinglePane(ADataGeneralBikeSinglePane<GeneralBike> generalBikeSinglePane) {
		JButton button = new JButton("Edit");
		generalBikeSinglePane.addDataHandlingComponent(button);

		IDataGeneralBikeManageController<GeneralBike> manageController = new IDataGeneralBikeManageController<GeneralBike>() {
			@Override
			public void update(GeneralBike t) {
				if (controller instanceof AdminGeneralBikePageController) {
					GeneralBike newGeneralBike = ((AdminGeneralBikePageController) controller).updateGeneralBike(t);
					generalBikeSinglePane.updateDataGeneralBike(newGeneralBike);
				}
			}

			@Override
			public void create(GeneralBike t) {
			}

			@Override
			public void read(GeneralBike t) {
			}

			@Override
			public void delete(GeneralBike t) {

			}
		};

		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				GeneralBike generalBike = generalBikeSinglePane.getDataGeneralBike();
				if (generalBike instanceof Bike) {
					new BikeEditDialog(generalBike, manageController);
				}
				if (generalBike instanceof EBike) {
					new EBikeEditDialog(generalBike, manageController);
				}
				if (generalBike instanceof TwinBike) {
					new TwinBikeEditDialog(generalBike, manageController);
				}
			}
		});
	}
}
