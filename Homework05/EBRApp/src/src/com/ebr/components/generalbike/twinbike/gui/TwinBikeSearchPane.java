package com.ebr.components.generalbike.twinbike.gui;

import java.util.Map;

import com.ebr.components.generalbike.gui.GeneralBikeSearchPane;

@SuppressWarnings("serial")
public class TwinBikeSearchPane extends GeneralBikeSearchPane {
	public TwinBikeSearchPane() {
		super();
	}

	@Override
	public void buildControls() {
		super.buildControls();
	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();

		return res;
	}
}
