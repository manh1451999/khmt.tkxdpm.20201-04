package com.ebr.components.generalbike.ebike.gui;

import javax.swing.JLabel;

import com.ebr.bean.EBike;
import com.ebr.bean.GeneralBike;
import com.ebr.components.generalbike.gui.GeneralBikeSinglePane;

@SuppressWarnings("serial")
public class EBikeSinglePane extends GeneralBikeSinglePane {
	private JLabel labelBatteryPercentage;
	private JLabel labelLoadCycles;
	private JLabel labelEstimatedUsageTimeRemaining;
	
	public EBikeSinglePane() {
		super();
	}

	public EBikeSinglePane(GeneralBike generalBike) {
		this();
		this.t = generalBike;

		displayDataGeneralBike();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelBatteryPercentage = new JLabel();
		add(labelBatteryPercentage, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelLoadCycles = new JLabel();
		add(labelLoadCycles, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelEstimatedUsageTimeRemaining = new JLabel();
		add(labelEstimatedUsageTimeRemaining, c);
	}

	@Override
	public void displayDataGeneralBike() {
		super.displayDataGeneralBike();
		
		if (t instanceof EBike) {
			EBike eBike = (EBike) t;
			
			labelBatteryPercentage.setText("Battery Percentage: " + eBike.getBatteryPercentage());
			labelLoadCycles.setText("Load Cycles: " + eBike.getLoadCycles());
			labelEstimatedUsageTimeRemaining.setText("Estimated Usage Time Remaining: " + eBike.getEstimatedUsageTimeRemaining());
		}
	}
}
