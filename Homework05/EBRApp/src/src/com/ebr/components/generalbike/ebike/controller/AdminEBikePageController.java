package com.ebr.components.generalbike.ebike.controller;

import java.util.List;
import java.util.Map;

import com.ebr.bean.EBike;
import com.ebr.bean.GeneralBike;
import com.ebr.components.generalbike.controller.AdminGeneralBikePageController;
import com.ebr.components.generalbike.ebike.gui.EBikeSearchPane;
import com.ebr.components.generalbike.ebike.gui.EBikeSinglePane;
import com.ebr.components.generalbike.gui.GeneralBikeSearchPane;
import com.ebr.components.generalbike.gui.GeneralBikeSinglePane;
import com.ebr.serverapi.GeneralBikeApi;

public class AdminEBikePageController extends AdminGeneralBikePageController {
	@Override
	public List<? extends GeneralBike> search(Map<String, String> searchParams) {
		return new GeneralBikeApi().getEBikes(searchParams);
	}

	@Override
	public GeneralBikeSinglePane createGeneralBikeSinglePane() {
		return new EBikeSinglePane();
	}

	@Override
	public GeneralBikeSearchPane createGeneralBikeSearchPane() {
		return new EBikeSearchPane();
	}

	@Override
	public GeneralBike updateGeneralBike(GeneralBike generalBike) {
		return new GeneralBikeApi().updateEBike((EBike) generalBike);
	}
}
