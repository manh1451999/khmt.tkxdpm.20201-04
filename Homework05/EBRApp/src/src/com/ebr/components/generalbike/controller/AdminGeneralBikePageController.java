package com.ebr.components.generalbike.controller;

import com.ebr.bean.GeneralBike;
import com.ebr.components.abstractdatageneralbike.controller.ADataGeneralBikePageController;
import com.ebr.components.abstractdatageneralbike.gui.ADataGeneralBikeListPane;
import com.ebr.components.generalbike.gui.AdminGeneralBikeListPane;

public abstract class AdminGeneralBikePageController extends ADataGeneralBikePageController<GeneralBike> {
	public AdminGeneralBikePageController() {
		super();
	}

	@Override
	public ADataGeneralBikeListPane<GeneralBike> createGeneralBikeListPane() {
		return new AdminGeneralBikeListPane(this);
	}

	public abstract GeneralBike updateGeneralBike(GeneralBike generalBike);
}
