package com.ebr.components.detailStation;



import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import com.ebr.bean.Bike;
import com.ebr.bean.SingleBike;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.abstractdata.gui.ADataListPane;
import com.ebr.components.abstractdata.gui.ADataPagePane;
import com.ebr.components.abstractdata.gui.ADataSearchPane;
import com.ebr.components.abstractdata.gui.ADataSinglePane;





public class SingleBikePageController extends ADataPageController<Bike> {
	protected BikeInStationPane<Bike> pagePane;
	protected ADataListPane<Bike> listPane;
	public SingleBikePageController(List<? extends Bike> list) {
		super(list);
		listPane = createListPane(list);
		pagePane = new BikeInStationPane<Bike>(listPane);
		// TODO Auto-generated constructor stub
	}

//	private CartController cartController;

	@Override
	public List<? extends SingleBike> search(Map<String, String> searchParams) {
		List<SingleBike> list = new ArrayList<SingleBike>();
		return list;
	}

	@Override
	public ADataSinglePane<Bike> createSinglePane() {
		// TODO Auto-generated method stub
		return new SingleBikeSinglePane();
	}
	
	public ADataListPane<Bike> createListPane(List<? extends Bike> list) {
		// TODO Auto-generated method stub
		SingleBikeListPane singleBikeListPane = new SingleBikeListPane(this);
		singleBikeListPane.displayData(list);
		return singleBikeListPane;
	}

	@Override
	public ADataSearchPane createSearchPane() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ADataListPane<Bike> createListPane() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public JPanel getDataPagePane() {
		return pagePane;
	}
	

}
