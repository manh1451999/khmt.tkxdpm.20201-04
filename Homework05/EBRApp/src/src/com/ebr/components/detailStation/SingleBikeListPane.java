package com.ebr.components.detailStation;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.ebr.bean.Bike;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.abstractdata.gui.ADataListPane;
import com.ebr.components.abstractdata.gui.ADataSinglePane;

import com.ebr.rentBike.controller.RentBikePageController;




public class SingleBikeListPane extends ADataListPane<Bike>{
	
	public SingleBikeListPane(ADataPageController<Bike> controller) {
		this.controller = controller;
	}
	
	@Override
	public void decorateSingleReturnPane(ADataSinglePane<Bike> singlePane) {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	public void decorateSinglePane(final ADataSinglePane<Bike> singlePane) {
		JButton button = new JButton("Thu� xe");
		singlePane.addDataHandlingComponent(button);
		
		button.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				Bike bike = singlePane.getData();
				RentBikePageController rentalBikeController = new RentBikePageController();
				rentalBikeController.rentalBikeDialog(bike);
			}
		});
	}
}
