package com.ebr.components.detailStation;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import com.ebr.bean.Bike;
import com.ebr.bean.SingleBike;
import com.ebr.bean.Station;
import com.ebr.serverapi.GeneralBikeApi;




@SuppressWarnings("serial")
public class DetailStationPage extends JFrame {

	public static final int WINDOW_WIDTH = 500;
	public static final int WINDOW_HEIGHT = 400;
	private GeneralBikeApi bikeApi = new GeneralBikeApi();
	
	private JTabbedPane tabbedPane;
	private List<SingleBike> singleBikeList = new ArrayList<SingleBike>();
	
	public DetailStationPage(Station station, DetailStationController controller) {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);
		
//		rootPanel.add(controller.getCartPane(), BorderLayout.NORTH);
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		JLabel nameStation = new JLabel(station.getName());
		panel.setSize(250, 30);
		panel.add(nameStation, BorderLayout.EAST);
		
		rootPanel.add(panel,BorderLayout.NORTH);
		
	    tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);
		
		
		ArrayList<String> listID= station.getListBikeId();
		List<Bike> listBike = new ArrayList<Bike>();
		for(String id: listID) {
			Map<String, String> param = new HashMap<String, String>();
			param.put("id", id);
			if(bikeApi.getBikes(param).size()>0) listBike.addAll(bikeApi.getBikes(param));	
		}
		
//		List<Bike> list = new ArrayList<Bike>();
//		list = bikeApi.getBikes(null);
//		
	
		
		JPanel singlePage = controller.getSingleBikePage(listBike);
		tabbedPane.addTab("Single Bike", null, singlePage, "Single Bike");
		
		tabbedPane.addTab("Couple Bike", null, new JPanel(), "Couple Bike");
		tabbedPane.addTab("Electric Bike", null, new JPanel(), "Electric Bike");


//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Chi tiết xe trong bãi");
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}
}
