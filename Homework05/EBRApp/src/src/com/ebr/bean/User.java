package com.ebr.bean;

import java.util.Date;

public class User {


	private String id;
	private String name;
	private float balanceAccount;


	public User() {
		super();
	}

	public User(String id, String name, float balanceAccount) {
		super();
		this.id = id;
		this.name = name;
		this.balanceAccount = balanceAccount;

	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getBalanceAccount() {
		return 650000;
	}

	public void setBalanceAccount(float balanceAccount) {
		this.balanceAccount = balanceAccount;
	}

}