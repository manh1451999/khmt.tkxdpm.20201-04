package com.ebr.rentBike.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import com.ebr.rentBike.controller.RentBikePageController;



public abstract class ADataRentBikePane<T> extends JDialog {
	protected T t;

	protected GridBagLayout layout;
	protected GridBagConstraints c;

	protected JPanel panel = new JPanel();
	protected JButton rentalButton;

	private RentBikePageController rentBikePageController;
	protected JLabel labelNotifi = new JLabel();

	public ADataRentBikePane() {
		buildControls();
	}

	public void setRentBikeController(RentBikePageController rentBikePageController) {
		this.rentBikePageController = rentBikePageController;
	}

	public ADataRentBikePane(T t) {
		this();
		this.t = t;

		displayData();
	}

	public void buildControls() {
		JDialog dialog = new JDialog();
		dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
		c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		
		getContentPane().setLayout(new BorderLayout());
		GridBagLayout gbl_contentPanel = new GridBagLayout();
		gbl_contentPanel.columnWeights = new double[] { 0.0, 1.0 };

		panel.setLayout(gbl_contentPanel);

		getContentPane().add(panel, BorderLayout.WEST);
	}

	public abstract void displayData();

	public void updateData(T t) {
		this.t = t;
		displayData();
	}

	public T getData() {
		return this.t;
	}

	protected int getLastRowIndex() {
		layout.layoutContainer(this);
		int[][] dim = layout.getLayoutDimensions();
		int rows = dim[1].length;
		return rows;
	}
}
