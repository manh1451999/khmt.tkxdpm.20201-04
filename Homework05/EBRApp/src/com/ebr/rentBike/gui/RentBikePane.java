package com.ebr.rentBike.gui;

import com.ebr.bean.Bike;

public class RentBikePane extends RentBikeGeneralPane {
	public RentBikePane() {
		super();
	}
	
	public RentBikePane(Bike bike) {
		this();
		this.t = bike;
		
		displayData();
	}
}
