package com.ebr.rentBike.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.ebr.bean.Bike;
import com.ebr.bean.EBike;
import com.ebr.bean.GeneralBike;
import com.ebr.serverapi.UserAPI;

import com.ebr.rentBike.controller.RentBikeController;

public class RentEBikePane extends RentBikeGeneralPane {
	private JLabel labelbatteryPercentage;
	private JLabel labelloadCycles;
	private JLabel labelestimatedUsageTimeRemaining;
	private UserAPI user;
	private EBike ebike;
	public RentEBikePane() {
		super();
	}
	
	public RentEBikePane(EBike ebike) {
		this();
		this.ebike = ebike;
		
		displayData();
	}
	
	@Override
	public void buildControls() {
		
		super.buildControls();
		setTitle("Th�ng tin thu� xe");
		setBounds(300, 300, 220, 250);
		labelbatteryPercentage = new JLabel();
		c.insets = new Insets(0, 5, 10, 0);
		c.gridx = 0;
		c.gridy = 8;
		panel.add(labelbatteryPercentage, c);

		labelloadCycles = new JLabel();
		c.gridx = 0;
		c.gridy = 9;
		panel.add(labelloadCycles, c);

		labelestimatedUsageTimeRemaining = new JLabel();
		c.gridx = 0;
		c.gridy = 10;
		panel.add(labelestimatedUsageTimeRemaining, c);
		
		
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		rentalButton = new JButton("Thanh to�n");
		buttonPane.add(rentalButton);
		rentalButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
					user = new UserAPI();
					GeneralBike bike = t;
					RentBikeController test =  new RentBikeController();
					test.checkRentBike(bike, user.getBalance());
					RentEBikePane.this.dispose();
//				}
			}
		});
	}
	@Override
	public void displayData() {
		// TODO Auto-generated method stub
		user = new UserAPI();
		labelbatteryPercentage.setText("Battery Percentage: " + ebike.getBatteryPercentage());
		labelloadCycles.setText("Load Cycles: " + ebike.getLoadCycles());
		labelestimatedUsageTimeRemaining.setText("Estimated Usage Time Remaining: " + ebike.getEstimatedUsageTimeRemaining() + "");
		
	}

}
