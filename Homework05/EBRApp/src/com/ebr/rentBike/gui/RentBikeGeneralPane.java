package com.ebr.rentBike.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;


import com.ebr.bean.GeneralBike;
import com.ebr.serverapi.UserAPI;

import com.ebr.rentBike.controller.RentBikeController;



public class RentBikeGeneralPane extends ADataRentBikePane<GeneralBike> {

	private JLabel labelName;
	private JLabel labelCost;
	private JLabel labelWeight;
	private JLabel labelLicensePlates;
	private JLabel labelManuafacturingDate;
	private JLabel labelProducer;
	private JLabel labelBalance;
	
	private UserAPI user;
//	protected RentalBikeApi rentalApi = new RentalBikeApi(); 
	
	public RentBikeGeneralPane() {
		super();
	}

	public RentBikeGeneralPane(GeneralBike generalbike) {
		this();
		this.t = generalbike;

		displayData();
	}

	@Override
	public void buildControls() {
		
		super.buildControls();
		setTitle("Thông tin thuê xe");
		setBounds(300, 300, 220, 250);
		labelName = new JLabel();
		c.insets = new Insets(0, 5, 10, 0);
		c.gridx = 0;
		c.gridy = 1;
		panel.add(labelName, c);

		labelWeight = new JLabel();
		c.gridx = 0;
		c.gridy = 2;
		panel.add(labelWeight, c);

		labelCost = new JLabel();
		c.gridx = 0;
		c.gridy = 3;
		panel.add(labelCost, c);
		
		labelLicensePlates = new JLabel();
		c.gridx = 0;
		c.gridy = 4;
		panel.add(labelLicensePlates, c);
		
		labelManuafacturingDate = new JLabel();
		c.gridx = 0;
		c.gridy = 5;
		panel.add(labelManuafacturingDate, c);
		
		labelProducer = new JLabel();
		c.gridx = 0;
		c.gridy = 6;
		panel.add(labelProducer, c);
		
		labelBalance = new JLabel();
		c.gridx = 0;
		c.gridy = 7;
		panel.add(labelBalance, c);
		
		JPanel buttonPane = new JPanel();
		buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
		getContentPane().add(buttonPane, BorderLayout.SOUTH);
		rentalButton = new JButton("Thanh toán");
		buttonPane.add(rentalButton);
		rentalButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
					user = new UserAPI();
					GeneralBike bike = t;
					RentBikeController test =  new RentBikeController();
					test.checkRentBike(bike, user.getBalance());
					RentBikeGeneralPane.this.dispose();
//				}
			}
		});
	}
	@Override
	public void displayData() {
		// TODO Auto-generated method stub
		user = new UserAPI();
		labelName.setText("Name: " + t.getName());
		labelWeight.setText("Weight: " + t.getWeight());
		labelCost.setText("Cost: " + t.getCost() + "");
		labelLicensePlates.setText("LicensePlates: " + t.getLicensePlate());
		labelManuafacturingDate.setText("Seate: " + t.getManuafacturingDate()+ "");
		labelProducer.setText("Cost: " + t.getCost() + "");
		labelBalance.setText("Balance: " + user.getBalance() + "");
		
	}

}
