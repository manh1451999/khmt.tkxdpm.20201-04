package com.ebr.rentBike.gui;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;

import com.ebr.bean.GeneralBike;
import com.ebr.bean.Rental;

import com.ebr.rentBike.controller.RentBikePageController;

public class NotificationRentBikePane extends ADataRentBikePane<Rental>{

	private JLabel labelNotifi;
	private Rental rental;
	
	private JLabel labelloaiXe;
	private JLabel labeltienCoc;
	private JLabel labelthoiGianBatDauThue;
	private JLabel labelmaXe;
	private JLabel labelBalance;
	private int check;
	private float tienDu;
	
	public NotificationRentBikePane() {
		buildControls();
	}


	public NotificationRentBikePane(Rental rental, int check, float tienDu) {
		this();
		this.rental = rental;
		this.check = check;
		this.tienDu = tienDu;
		displayData();
	}

	public void buildControls() {

		super.buildControls();
		setTitle("ThÃ´ng tin hÃ³a Ä‘Æ¡n");
		setBounds(300, 300, 300, 200);

		labelNotifi = new JLabel();
		c.insets = new Insets(0, 5, 10, 0);
		c.gridx = 0;
		c.gridy = 0;
		panel.add(labelNotifi, c);

		
		labelmaXe = new JLabel();
		c.gridx = 0;
		c.gridy = 1;
		panel.add(labelmaXe, c);
		
		labelloaiXe = new JLabel();
		c.gridx = 0;
		c.gridy = 2;
		panel.add(labelloaiXe, c);
		
		labeltienCoc = new JLabel();
		c.gridx = 0;
		c.gridy = 3;
		panel.add(labeltienCoc, c);
		
		labelthoiGianBatDauThue = new JLabel();
		c.gridx = 0;
		c.gridy = 4;
		panel.add(labelthoiGianBatDauThue, c);
		
		labelBalance = new JLabel();
		c.gridx = 0;
		c.gridy = 5;
		panel.add(labelBalance, c);


	}
	
	public void displayData() {
		// TODO Auto-generated method stub
		if (check == 0 ) {
			labelNotifi.setText("Sá»‘ dÆ° hiá»‡n khÃ´ng Ä‘á»§ vui lÃ²ng náº¡p thÃªm tiá»�n");
		} else {
			labelNotifi.setText("---- ThÃ nh cÃ´ng ----" );
			labelmaXe.setText("MÃ£ xe: " + rental.getMaXe() +"");
			labelloaiXe.setText("Loáº¡i xe: " + rental.getLoaiXe() +"");
			labeltienCoc.setText("Tiá»�n cá»�c: " + rental.getTienCoc() + "");
			labelthoiGianBatDauThue.setText("Thá»�i gian báº¯t Ä‘áº§u: " + rental.getThoiGianBatDauThue() + "");
			labelBalance.setText("Sá»‘ dÆ° cÃ²n láº¡i: " + tienDu + "");
		}	
	}
}
