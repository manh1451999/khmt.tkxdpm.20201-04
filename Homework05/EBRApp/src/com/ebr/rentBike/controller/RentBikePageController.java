package com.ebr.rentBike.controller;

import com.ebr.bean.Bike;
import com.ebr.bean.GeneralBike;

import com.ebr.rentBike.gui.RentBikeGeneralPane;
import com.ebr.rentBike.gui.RentBikePane;

public class RentBikePageController {
	private RentBikeGeneralPane rentBikeGeneralPane;
	private GeneralBike generalBike;
	public RentBikePageController() {
		super();
		generalBike = new GeneralBike();
	}

	public void setBikePane(RentBikeGeneralPane generakBikePane) {
		this.rentBikeGeneralPane = generakBikePane;
	}
	
	public void rentalBikeDialog(Bike BikePane) {
		rentBikeGeneralPane = new RentBikePane(BikePane);
		rentBikeGeneralPane.setVisible(true);
	}
	

}
