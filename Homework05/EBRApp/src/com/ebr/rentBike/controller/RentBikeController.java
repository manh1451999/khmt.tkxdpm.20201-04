package com.ebr.rentBike.controller;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import com.ebr.bean.GeneralBike;
import com.ebr.bean.Rental;

import com.ebr.rentBike.gui.NotificationRentBikePane;

public class RentBikeController {
	private Rental rental;
	public RentBikeController() {
		super();
	}
	
	public void checkRentBike(GeneralBike generalBike, float balance) {
		if(generalBike.getCost() > balance) {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy");
			LocalDateTime startAtNow = LocalDateTime.now();
			String startAt = startAtNow.format(formatter);
			NotificationRentBikePane test = new NotificationRentBikePane(null, 0, balance);
			test.setVisible(true);
		} else {
			DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy");
			LocalDateTime startAtNow = LocalDateTime.now();
			String startAt = startAtNow.format(formatter);
			float tienDu = (float) balance - generalBike.getCost();
			rental = new Rental("", "", generalBike.getBikeType(), generalBike.getId(), generalBike.getCost(), 0, startAt, 0);
			NotificationRentBikePane test = new NotificationRentBikePane(rental, 1, tienDu);
			test.setVisible(true);
		}
	}
}
