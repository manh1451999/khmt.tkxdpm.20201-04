package com.ebr.serverapi;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.bean.Station;

public class StationApi {
	public static final String PATH = "http://localhost:8080/";
	
	private static Client client;
	
	public StationApi() {
		client = ClientBuilder.newClient();
	}
	
	public ArrayList<Station> getAllStations() {
		WebTarget webTarget = client.target(PATH).path("stations");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>(){});
		System.out.println("get all "+ webTarget);
		return res;
	}
	
	public ArrayList<Station> getStations(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("stations/search");
		System.out.println("search "+ webTarget);

		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Station> res = response.readEntity(new GenericType<ArrayList<Station>>() {});
		System.out.println("Helelo my fen"+queryParams);
		return res;
	}
	
	public Station updateStation(Station station) {
		WebTarget webTarget = client.target(PATH).path("stations").path(station.getId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(station, MediaType.APPLICATION_JSON));
		
		Station res = response.readEntity(Station.class);
		return res;
	}
	public static String addStation(Station station) {
		ArrayList<String> list = new ArrayList<String>();
	//	station.setId(id);
		station.setDistance(0);
		station.setListBikeId(list);
		station.setListEBikeId(list);
		station.setListTwinBikeId(list);
		station.setTime(0);
		WebTarget webTarget = client.target(PATH).path("stations");
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(station, MediaType.APPLICATION_JSON));
		//System.out.println(response.getCookies());
	 String res = response.readEntity(String.class);
		 System.out.println(res);
		 return res;
	}
	
}
