package com.ebr.serverapi;

import java.util.ArrayList;
import java.util.Map;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ebr.bean.Bike;
import com.ebr.bean.EBike;
import com.ebr.bean.GeneralBike;
import com.ebr.bean.TwinBike;

public class GeneralBikeApi {
	public static final String PATH = "http://localhost:8080/";
	
	private Client client;
	
	public GeneralBikeApi() {
		client = ClientBuilder.newClient();
	}
	
	public ArrayList<GeneralBike> getAllGeneralBikes() {
		WebTarget webTarget = client.target(PATH).path("generalbikes");

		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();

		ArrayList<GeneralBike> res = response.readEntity(new GenericType<ArrayList<GeneralBike>>(){});
//		System.out.println(res);
		return res;
	}
	
	public ArrayList<Bike> getBikes(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("bikes");
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<Bike> res = response.readEntity(new GenericType<ArrayList<Bike>>() {});
//		System.out.println(res);
		return res;
	}
	
	public Bike updateBike(Bike bike) {
		WebTarget webTarget = client.target(PATH).path("bikes").path(bike.getId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(bike, MediaType.APPLICATION_JSON));
		
		Bike res = response.readEntity(Bike.class);
		return res;
	}
	
	public ArrayList<EBike> getEBikes(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("ebikes");
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<EBike> res = response.readEntity(new GenericType<ArrayList<EBike>>() {});
//		System.out.println(res);
		return res;
	}
	
	public EBike updateEBike(EBike eBike) {
		WebTarget webTarget = client.target(PATH).path("ebikes").path(eBike.getId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(eBike, MediaType.APPLICATION_JSON));
		
		EBike res = response.readEntity(EBike.class);
		return res;
	}
	
	public ArrayList<TwinBike> getTwinBikes(Map<String, String> queryParams) {
		WebTarget webTarget = client.target(PATH).path("twinbikes");
		
		if (queryParams != null) {
			for (String key : queryParams.keySet()) {
				String value = queryParams.get(key);
				webTarget = webTarget.queryParam(key, value);
			}
		}
		
		Invocation.Builder invocationBuilder = webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.get();
		ArrayList<TwinBike> res = response.readEntity(new GenericType<ArrayList<TwinBike>>() {});
//		System.out.println(res);
		return res;
	}
	
	public TwinBike updateTwinBike(TwinBike twinBike) {
		WebTarget webTarget = client.target(PATH).path("twinbikes").path(twinBike.getId());
		
		Invocation.Builder invocationBuilder =  webTarget.request(MediaType.APPLICATION_JSON);
		Response response = invocationBuilder.post(Entity.entity(twinBike, MediaType.APPLICATION_JSON));
		
		TwinBike res = response.readEntity(TwinBike.class);
		return res;
	}
}
