package com.ebr.app.admin;

import javax.swing.JPanel;

import com.ebr.bean.GeneralBike;
import com.ebr.bean.Station;
import com.ebr.components.abstractdatageneralbike.controller.ADataGeneralBikePageController;
import com.ebr.components.abstractdatastation.controller.ADataStationPageController;
import com.ebr.components.generalbike.bike.controller.AdminBikePageController;
import com.ebr.components.generalbike.ebike.controller.AdminEBikePageController;
import com.ebr.components.generalbike.twinbike.controller.AdminTwinBikePageController;
import com.ebr.components.station.controller.AdminStationPageController;

public class EBRAdminController {
	public JPanel getStationPage() {
		ADataStationPageController<Station> controller = new AdminStationPageController();
		return controller.getDataStationPagePane();
	}

	public JPanel getBikePage() {
		ADataGeneralBikePageController<GeneralBike> controller = new AdminBikePageController();
		return controller.getDataGeneralBikePagePane();
	}

	public JPanel getEBikePage() {
		ADataGeneralBikePageController<GeneralBike> controller = new AdminEBikePageController();
		return controller.getDataGeneralBikePagePane();
	}

	public JPanel getTwinBikePage() {
		ADataGeneralBikePageController<GeneralBike> controller = new AdminTwinBikePageController();
		return controller.getDataGeneralBikePagePane();
	}
}
