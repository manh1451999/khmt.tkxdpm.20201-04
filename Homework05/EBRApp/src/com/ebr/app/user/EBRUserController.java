package com.ebr.app.user;

import javax.swing.JPanel;

import com.ebr.components.station.controller.UserStationPageController;



public class EBRUserController {
//	private CartController cartController;
	
	public EBRUserController() {
//		cartController = new CartController();
	}
	
//	public JPanel getCartPane() {
//		return cartController.getCartPane();
//	}
	
	public JPanel getStationPage() {
		UserStationPageController controller = new UserStationPageController();
//		controller.setCartController(cartController);
		return controller.getDataPagePane();
	}

	public JPanel getReturnStationPage() {
		UserStationPageController controller = new UserStationPageController();
		return controller.getReturnDataPagePane();
	}
}
