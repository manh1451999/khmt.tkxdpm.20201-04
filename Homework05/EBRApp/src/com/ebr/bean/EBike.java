package com.ebr.bean;

import java.util.Date;

public class EBike extends GeneralBike {
	private float batteryPercentage;
	private int loadCycles;
	private int estimatedUsageTimeRemaining; // thời gian tính bằng s

	public EBike() {
		super();
	}

	public EBike(String id, String name, String bikeType, float weight, String licensePlate, Date manuafacturingDate,
			String producer, float cost) {
		super(id, name, bikeType, weight, licensePlate, manuafacturingDate, producer, cost);
	}

	public EBike(String id, String name, String bikeType, float weight, String licensePlate, Date manuafacturingDate,
			String producer, float cost, float batteryPercentage, int loadCycles, int estimatedUsageTimeRemaining) {
		super(id, name, bikeType, weight, licensePlate, manuafacturingDate, producer, cost);

		this.batteryPercentage = batteryPercentage;
		this.loadCycles = loadCycles;
		this.estimatedUsageTimeRemaining = estimatedUsageTimeRemaining;
	}

	public float getBatteryPercentage() {
		return batteryPercentage;
	}

	public void setBatteryPercentage(float batteryPercentage) {
		this.batteryPercentage = batteryPercentage;
	}

	public int getLoadCycles() {
		return loadCycles;
	}

	public void setLoadCycles(int loadCycles) {
		this.loadCycles = loadCycles;
	}

	public int getEstimatedUsageTimeRemaining() {
		return estimatedUsageTimeRemaining;
	}

	public void setEstimatedUsageTimeRemaining(int estimatedUsageTimeRemaining) {
		this.estimatedUsageTimeRemaining = estimatedUsageTimeRemaining;
	}

	@Override
	public boolean match(GeneralBike generalBike) {
		if (generalBike == null) {
			return true;
		}

		boolean res = super.match(generalBike);
		if (!res) {
			return false;
		}

		if (!(generalBike instanceof EBike)) {
			return false;
		}

		EBike eBike = (EBike) generalBike;

		if (eBike.batteryPercentage != 0 && this.batteryPercentage != eBike.batteryPercentage) {
			return false;
		}
		if (eBike.loadCycles != 0 && this.loadCycles != eBike.loadCycles) {
			return false;
		}
		if (eBike.estimatedUsageTimeRemaining != 0
				&& this.estimatedUsageTimeRemaining != eBike.estimatedUsageTimeRemaining) {
			return false;
		}

		return true;
	}
}
