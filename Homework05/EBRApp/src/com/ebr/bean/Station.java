package com.ebr.bean;

import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("station")
public class Station {
	private String id;
	private String name;
	private String address;
	private ArrayList<String> listBikeId;
	private ArrayList<String> listEBikeId;
	private ArrayList<String> listTwinBikeId;
//	private int numberOfBikes;
//	private int numberOfEBikes;
//	private int numberOfTwinBikes;
	private int numberOfEmptyDocks;
	private float distance;
	private int time;

	public Station() {
		listBikeId = new ArrayList<String>();
		listEBikeId = new ArrayList<String>();
		listTwinBikeId = new ArrayList<String>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public ArrayList<String> getListBikeId() {
		return listBikeId;
	}

	public void setListBikeId(ArrayList<String> listBikeId) {
		this.listBikeId = listBikeId;
	}

	public ArrayList<String> getListEBikeId() {
		return listEBikeId;
	}

	public void setListEBikeId(ArrayList<String> listEBikeId) {
		this.listEBikeId = listEBikeId;
	}

	public ArrayList<String> getListTwinBikeId() {
		return listTwinBikeId;
	}

	public void setListTwinBikeId(ArrayList<String> listTwinBikeId) {
		this.listTwinBikeId = listTwinBikeId;
	}

	public int getNumberOfEmptyDocks() {
		return numberOfEmptyDocks;
	}

	public void setNumberOfEmptyDocks(int numberOfEmptyDocks) {
		this.numberOfEmptyDocks = numberOfEmptyDocks;
	}

	public float getDistance() {
		return distance;
	}

	public void setDistance(float distance) {
		this.distance = distance;
	}

	public int getTime() {
		return time;
	}

	public void setTime(int time) {
		this.time = time;
	}

	public boolean match(Station station) {
		if (station == null) {
			return true;
		}

		if (station.id != null && !station.id.equals("") && !this.id.contains(station.id)) {
			return false;
		}
		if (station.name != null && !station.name.equals("") && !this.name.contains(station.name)) {
			return false;
		}
		if (station.address != null && !station.address.equals("") && !this.address.contains(station.address)) {
			return false;
		}

		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Station) {
			return this.id.equals(((Station) obj).id);
		}
		return false;
	}
}
