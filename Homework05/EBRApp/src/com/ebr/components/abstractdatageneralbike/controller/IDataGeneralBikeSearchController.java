package com.ebr.components.abstractdatageneralbike.controller;

import java.util.Map;

public interface IDataGeneralBikeSearchController {
	public void search(Map<String, String> searchParams);
}
