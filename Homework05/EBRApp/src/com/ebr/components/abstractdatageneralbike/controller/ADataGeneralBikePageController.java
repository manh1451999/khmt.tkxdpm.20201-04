package com.ebr.components.abstractdatageneralbike.controller;

import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import com.ebr.components.abstractdatageneralbike.gui.ADataGeneralBikeListPane;
import com.ebr.components.abstractdatageneralbike.gui.ADataGeneralBikePagePane;
import com.ebr.components.abstractdatageneralbike.gui.ADataGeneralBikeSearchPane;
import com.ebr.components.abstractdatageneralbike.gui.ADataGeneralBikeSinglePane;

public abstract class ADataGeneralBikePageController<T> {
	private ADataGeneralBikePagePane<T> generalBikePagePane;

	public ADataGeneralBikePageController() {
		ADataGeneralBikeSearchPane generalBikeSearchPane = createGeneralBikeSearchPane();

		ADataGeneralBikeListPane<T> generalBikeListPane = createGeneralBikeListPane();

		generalBikeSearchPane.setController(new IDataGeneralBikeSearchController() {
			@Override
			public void search(Map<String, String> searchParams) {
				List<? extends T> list = ADataGeneralBikePageController.this.search(searchParams);
				generalBikeListPane.updateDataGeneralBike(list);
			}
		});

		generalBikeSearchPane.fireSearchEvent();

		generalBikePagePane = new ADataGeneralBikePagePane<T>(generalBikeSearchPane, generalBikeListPane);
	}

	public JPanel getDataGeneralBikePagePane() {
		return generalBikePagePane;
	}

	public abstract ADataGeneralBikeSearchPane createGeneralBikeSearchPane();

	public abstract List<? extends T> search(Map<String, String> searchParams);

	public abstract ADataGeneralBikeSinglePane<T> createGeneralBikeSinglePane();

	public abstract ADataGeneralBikeListPane<T> createGeneralBikeListPane();
}
