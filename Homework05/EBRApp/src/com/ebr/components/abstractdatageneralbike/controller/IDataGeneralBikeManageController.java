package com.ebr.components.abstractdatageneralbike.controller;

public interface IDataGeneralBikeManageController<T> {
	public void create(T t);

	public void read(T t);

	public void delete(T t);

	public void update(T t);
}
