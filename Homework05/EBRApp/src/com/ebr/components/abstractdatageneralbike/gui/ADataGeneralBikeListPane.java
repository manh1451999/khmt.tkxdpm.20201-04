package com.ebr.components.abstractdatageneralbike.gui;

import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.ebr.components.abstractdatageneralbike.controller.ADataGeneralBikePageController;

@SuppressWarnings("serial")
public abstract class ADataGeneralBikeListPane<T> extends JScrollPane {
	private LayoutManager layout;
	protected JPanel pane;

	protected ADataGeneralBikePageController<T> controller;

	public ADataGeneralBikeListPane() {
		pane = new JPanel();
		layout = new BoxLayout(pane, BoxLayout.Y_AXIS);
		pane.setLayout(layout);

		this.setViewportView(pane);
		this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.getVerticalScrollBar().setUnitIncrement(20);
		this.getHorizontalScrollBar().setUnitIncrement(20);
	}

	public abstract void decorateGeneralBikeSinglePane(ADataGeneralBikeSinglePane<T> singlePane);

	public void updateDataGeneralBike(List<? extends T> list) {
		pane.removeAll();
		pane.revalidate();
		pane.repaint();

		for (T t : list) {
			ADataGeneralBikeSinglePane<T> generalBikeSinglePane = controller.createGeneralBikeSinglePane();
			decorateGeneralBikeSinglePane(generalBikeSinglePane);

			generalBikeSinglePane.updateDataGeneralBike(t);
			pane.add(generalBikeSinglePane);
			pane.add(Box.createRigidArea(new Dimension(0, 40)));
		}
	}
}
