package com.ebr.components.station.controller;

import java.util.List;
import java.util.Map;

import com.ebr.bean.Station;
import com.ebr.components.abstractdatastation.controller.ADataStationPageController;
import com.ebr.components.abstractdatastation.controller.IDataStationManageController;
import com.ebr.components.abstractdatastation.gui.ADataStationListPane;

import com.ebr.components.station.gui.StationSearchPane;
import com.ebr.components.station.gui.StationSinglePane;
import com.ebr.components.station.gui.AdminStationListPane;
import com.ebr.serverapi.StationApi;

public class AdminStationPageController<T> extends ADataStationPageController<Station> implements IDataStationManageController<T> {
	public AdminStationPageController() {
		super();
	}
	
	@Override
	public ADataStationListPane<Station> createStationListPane() {
		return new AdminStationListPane(this);
	}
	
	@Override
	public List<? extends Station> search(Map<String, String> searchParams) {
		return new StationApi().getStations(searchParams);
	}
	
	@Override
	public StationSinglePane createStationSinglePane() {
		return new StationSinglePane();
	}
	
	@Override
	public StationSearchPane createStationSearchPane() {
		return new StationSearchPane();
	}
	
	public Station updateStation(Station station) {
		return new StationApi().updateStation((Station) station);
	}

	@Override
	public String create(T t) {
		System.out.println("apiiii"+ t);
		Station station = (Station) t;
		 String res = new StationApi().addStation(station);
		 return res;
	}

	@Override
	public void read(T t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void delete(T t) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(T t) {
		// TODO Auto-generated method stub
		
	}
}
