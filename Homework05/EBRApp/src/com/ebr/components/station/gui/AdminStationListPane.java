package com.ebr.components.station.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import com.ebr.bean.Station;
import com.ebr.components.abstractdatastation.controller.ADataStationPageController;
import com.ebr.components.abstractdatastation.controller.IDataStationManageController;
import com.ebr.components.abstractdatastation.gui.ADataStationListPane;
import com.ebr.components.abstractdatastation.gui.ADataStationSinglePane;
import com.ebr.components.generalbike.bike.gui.BikeEditDialog;
import com.ebr.components.station.controller.AdminStationPageController;

@SuppressWarnings("serial")
public class AdminStationListPane extends ADataStationListPane<Station> {
	public AdminStationListPane(ADataStationPageController<Station> controller) {
		this.controller = controller;
	}
	private boolean add = true;
	@Override
	public void decorateStationSinglePane(ADataStationSinglePane<Station> stationSinglePane) {
		JButton button = new JButton("Edit");
		stationSinglePane.addDataHandlingComponent(button);
		
		IDataStationManageController<Station> manageController = new IDataStationManageController<Station>() {
			@Override
			public void update(Station t) {
				if (controller instanceof AdminStationPageController) {
					Station newStation= ((AdminStationPageController) controller).updateStation(t);
					stationSinglePane.updateDataStation(newStation);
				}
			}

			@Override
			public String create(Station t) {
				add= true;
				if (controller instanceof AdminStationPageController) {
				String res =  ((AdminStationPageController) controller).create(t);
				JOptionPane.showMessageDialog(stationSinglePane,
					    res);
				return res;
				}
				return null;
			}

			@Override
			public void read(Station t) {
			}

			@Override
			public void delete(Station t) {
				
			}
		};
		if(add) {
			JButton addButton = new JButton("Add new station");
			addButton.setName("add");
			stationSinglePane.addDataHandlingComponent(addButton);
			add = false;
			addButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					new StationCreatePane(manageController);
				}
			});	
		}
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				new StationEditDialog(stationSinglePane.getDataStation(), manageController);
			}
		});	
	}
}
