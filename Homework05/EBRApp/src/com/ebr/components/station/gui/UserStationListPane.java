package com.ebr.components.station.gui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

import com.ebr.bean.Station;
import com.ebr.components.abstractdata.controller.ADataPageController;
import com.ebr.components.abstractdata.gui.ADataListPane;
import com.ebr.components.abstractdata.gui.ADataSinglePane;
import com.ebr.components.detailStation.DetailStationController;
import com.ebr.components.detailStation.DetailStationPage;
import com.ebr.components.returnbike.DetailReturnBike;
import com.ebr.components.returnbike.DetailReturnController;

@SuppressWarnings("serial")
public class UserStationListPane extends ADataListPane<Station>{
	
	public UserStationListPane(ADataPageController<Station> controller) {
		this.controller = controller;
	}
	
	@Override
	public void decorateSingleReturnPane(ADataSinglePane<Station> singlePane) {
		// TODO Auto-generated method stub
		JButton button = new JButton("Tra xe ở bai nay");
		singlePane.addDataHandlingComponent(button);
		
		System.out.println("quang");
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(singlePane.getData());
				System.out.println("Tra xe o bai nay");
				new DetailReturnBike(singlePane.getData(), new DetailReturnController());
			}
		});
	}

	@Override
	public void decorateSinglePane(ADataSinglePane<Station> singlePane) {
//		JSpinner spin = new JSpinner();
//		spin.setModel(new SpinnerNumberModel(1, 0, null, 1));
//		singlePane.addDataHandlingComponent(spin);
//		spin.setPreferredSize(new Dimension(100, 20));
		
		JButton button = new JButton("Xem chi tiáº¿t xe trong bÃ£i");
		singlePane.addDataHandlingComponent(button);
		
		button.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println(singlePane.getData());
				System.out.println("Xem chi tiáº¿t xe trong bÃ£i");
				new DetailStationPage(singlePane.getData(), new DetailStationController());
			}
		});
	}
}
