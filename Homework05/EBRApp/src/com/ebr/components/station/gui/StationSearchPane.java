package com.ebr.components.station.gui;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ebr.components.abstractdatastation.gui.ADataStationSearchPane;

@SuppressWarnings("serial")
public class StationSearchPane extends ADataStationSearchPane {
	private JTextField nameField;
	private JTextField addressField;
//	private JTextField emptyField;
	
	public StationSearchPane() {
		super();
	}

	@Override
	public void buildControls() {
		JLabel nameLabel = new JLabel("Station Name");
		nameField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(nameLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(nameField, c);
		
		JLabel addressLabel = new JLabel("Address");
		addressField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(addressLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(addressField, c);
		
//		JLabel emptyLabel = new JLabel("Empty Docks");
//		emptyField = new JTextField(15);
//		row = getLastRowIndex();
//		c.gridx = 0;
//		c.gridy = row;
//		add(emptyLabel, c);
//		c.gridx = 1;
//		c.gridy = row;
//		add(emptyField, c);
		
	}
	
	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		if (!nameField.getText().trim().equals("")) {
			res.put("name", nameField.getText().trim());
		}
		if (!addressField.getText().trim().equals("")) {
			res.put("address", addressField.getText().trim());
		}
//		if (!emptyField.getText().trim().equals("")) {
//			res.put("numberOfEmptyDocks", emptyField.getText().trim());
//		}
		
		return res;
	}
}
