package com.ebr.components.station.gui;

import javax.swing.JLabel;

import com.ebr.bean.Station;
import com.ebr.components.abstractdatastation.gui.ADataStationSinglePane;

@SuppressWarnings("serial")
public class StationSinglePane extends ADataStationSinglePane<Station> {
	private JLabel labelName;
	private JLabel labelAddress;
	private JLabel labelEmptyDocks;
	
	public StationSinglePane() {
		super();
	}
		
	public StationSinglePane(Station station) {
		this();
		this.t = station;
		
		displayDataStation();
	}

	@Override
	public void buildControls() {
		super.buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelName = new JLabel();
		add(labelName, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelAddress = new JLabel();
		add(labelAddress, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelEmptyDocks = new JLabel();
		add(labelEmptyDocks, c);
	}
	
	
	@Override
	public void displayDataStation() {
		labelName.setText("Station Name: " + t.getName());
		labelAddress.setText("Address: " + t.getAddress());
		labelEmptyDocks.setText("Empty Docks: " + t.getNumberOfEmptyDocks());
//		labelManuafacturingDate.setText("Manuafacturing Date: " + t.getNumberOfEmptyDocks());
//		labelProducer.setText("Producer: " + t.getProducer());
//		labelCost.setText("Cost: " + t.getCost());
	}
}
