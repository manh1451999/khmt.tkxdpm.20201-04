package com.ebr.components.returnbike;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;

import com.ebr.bean.Bike;
import com.ebr.bean.Rental;
import com.ebr.bean.SingleBike;
import com.ebr.bean.Station;
import com.ebr.serverapi.GeneralBikeApi;
import com.ebr.serverapi.RentalApi;

@SuppressWarnings("serial")
public class DetailReturnBike extends JFrame{

	public static final int WINDOW_WIDTH = 500;
	public static final int WINDOW_HEIGHT = 400;
	private RentalApi rentalApi = new RentalApi();
	
	private JTabbedPane tabbedPane;
	private List<SingleBike> singleBikeList = new ArrayList<SingleBike>();
	
	public DetailReturnBike(Station station, DetailReturnController controller) {
		JPanel rootPanel = new JPanel();
		setContentPane(rootPanel);
		BorderLayout layout = new BorderLayout();
		rootPanel.setLayout(layout);
		
		JPanel panel = new JPanel();
		panel.setLayout(new BorderLayout());
		JLabel nameStation = new JLabel(station.getName());
		panel.setSize(250, 30);
		panel.add(nameStation, BorderLayout.EAST);
		
		rootPanel.add(panel,BorderLayout.NORTH);
		
	    tabbedPane = new JTabbedPane();
		rootPanel.add(tabbedPane, BorderLayout.CENTER);
		
		List<Rental> list = new ArrayList<Rental>();
		list = rentalApi.getRental(null);
		
		JPanel singlePage = controller.getSingleReturnPage(list);
		tabbedPane.addTab("", null, singlePage, "Single Bike");

//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Trả xe tại bãi " + station.getName());
		setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
		setVisible(true);
	}

}
