package com.ebr.components.returnbike;

import java.util.List;

import javax.swing.JPanel;

import com.ebr.bean.Bike;
import com.ebr.bean.Rental;
import com.ebr.components.detailStation.SingleBikePageController;

public class ReturnBikeController{

	public ReturnBikeController() {
		// TODO Auto-generated constructor stub
	}
	
	public JPanel getSingleBikePage(List<Rental> list) {
		SingleReturnPageController controller = new SingleReturnPageController(list);
		return controller.getDataPagePane();
	}
	
	
}
