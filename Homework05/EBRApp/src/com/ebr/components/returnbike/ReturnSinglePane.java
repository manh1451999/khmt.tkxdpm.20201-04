package com.ebr.components.returnbike;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.swing.JLabel;

import com.ebr.bean.Bike;
import com.ebr.bean.Rental;
import com.ebr.components.abstractdata.gui.ADataSinglePane;


public class ReturnSinglePane extends ADataSinglePane<Rental>{
	

	private JLabel lbId;
	private JLabel lbLoaiXe;
	private JLabel lbMaXe;
	private JLabel lbTienCoc;
	private JLabel lbThoiGianBatDauThue;
	private JLabel lbThoiGianThue;
	private JLabel lbIDBaiXe;
	private JLabel lbTienTra;
	
	@Override
	public void buildControls() {
		// TODO Auto-generated method stub
		super.buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		lbId = new JLabel();
		add(lbId, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		lbLoaiXe = new JLabel();
		add(lbLoaiXe, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		lbMaXe = new JLabel();
		add(lbMaXe, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		lbTienCoc = new JLabel();
		add(lbTienCoc, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		lbThoiGianBatDauThue = new JLabel();
		add(lbThoiGianBatDauThue, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		lbThoiGianThue = new JLabel();
		add(lbThoiGianThue, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		lbTienTra = new JLabel();
		add(lbTienTra, c);
	}
	
	@Override
	public void displayData() {
		// TODO Auto-generated method stub
		lbId.setText("id: " + t.getId());
		lbLoaiXe.setText("Loai xe: " + t.getLoaiXe());
		lbMaXe.setText("Ma xe: " + t.getMaXe());
		lbTienCoc.setText("Tien coc: "+ t.getTienCoc());
		lbThoiGianBatDauThue.setText("Thue tu: " + t.getThoiGianBatDauThue());
		
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm:ss dd-MM-yyyy");
        // get end time and set for rental
        LocalDateTime endAt =  LocalDateTime.now();
        //get start time
        LocalDateTime startAt = LocalDateTime.parse(t.getThoiGianBatDauThue(), formatter); 
            
        Duration duration = Duration.between(startAt, endAt);
        float longRent = (float)(duration.toMinutes());
		float tienTra = 0;
		if(longRent <= 10) {
			tienTra = 0;
		} else if(longRent <= 30){
            tienTra = 10000;
        } else {
        	tienTra = (float)(10000 + 3000 * Math.ceil((longRent - 30)/15));
        }
		
        if(t.getLoaiXe() != "Bike") {
        	tienTra = (float)(tienTra * 1.5);
        }
		
		
		
		lbThoiGianThue.setText("Thoi gian thue: " + longRent + "(phut)");
//		lbIDBaiXe.setText("Id bai xe: " + t.getIdBaiXe());
		lbTienTra.setText("Tien tra: " + tienTra);
	
	}

}
