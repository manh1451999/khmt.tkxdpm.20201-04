package com.ebr.components.generalbike.gui;

import javax.swing.JLabel;

import com.ebr.bean.GeneralBike;
import com.ebr.components.abstractdatageneralbike.gui.ADataGeneralBikeSinglePane;

@SuppressWarnings("serial")
public class GeneralBikeSinglePane extends ADataGeneralBikeSinglePane<GeneralBike> {
	private JLabel labelName;
	private JLabel labelWeight;
	private JLabel labelLicensePlate;
	private JLabel labelManuafacturingDate;
	private JLabel labelProducer;
	private JLabel labelCost;

	public GeneralBikeSinglePane() {
		super();
	}

	public GeneralBikeSinglePane(GeneralBike generalBike) {
		this();
		this.t = generalBike;

		displayDataGeneralBike();
	}

	@Override
	public void buildControls() {
		super.buildControls();

		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelName = new JLabel();
		add(labelName, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelWeight = new JLabel();
		add(labelWeight, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelLicensePlate = new JLabel();
		add(labelLicensePlate, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelManuafacturingDate = new JLabel();
		add(labelManuafacturingDate, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelProducer = new JLabel();
		add(labelProducer, c);

		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelCost = new JLabel();
		add(labelCost, c);
	}

	@Override
	public void displayDataGeneralBike() {
		labelName.setText("Name: " + t.getName());
		labelWeight.setText("Weight: " + t.getWeight());
		labelLicensePlate.setText("License Plate: " + t.getLicensePlate());
		labelManuafacturingDate.setText("Manuafacturing Date: " + t.getManuafacturingDate());
		labelProducer.setText("Producer: " + t.getProducer());
		labelCost.setText("Cost: " + t.getCost());
	}
}
