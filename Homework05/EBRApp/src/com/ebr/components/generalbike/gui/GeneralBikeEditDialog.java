package com.ebr.components.generalbike.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ebr.bean.GeneralBike;
import com.ebr.components.abstractdatageneralbike.controller.IDataGeneralBikeManageController;
import com.ebr.components.abstractdatageneralbike.gui.ADataGeneralBikeEditDialog;

@SuppressWarnings("serial")
public class GeneralBikeEditDialog extends ADataGeneralBikeEditDialog<GeneralBike> {
	private JTextField nameField;
	private JTextField weightField;
	private JTextField licensePlateField;
	private JTextField producerField;
	private JTextField costField;

	public GeneralBikeEditDialog(GeneralBike generalBike, IDataGeneralBikeManageController<GeneralBike> controller) {
		super(generalBike, controller);
	}

	@Override
	public void buildControls() {
		int row = getLastRowIndex();
		JLabel nameLabel = new JLabel("Name");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(nameLabel, c);
		nameField = new JTextField(15);
		nameField.setText(t.getName());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(nameField, c);

		row = getLastRowIndex();
		JLabel weightLabel = new JLabel("Weight");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(weightLabel, c);
		weightField = new JTextField(15);
		weightField.setText(t.getWeight() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(weightField, c);

		row = getLastRowIndex();
		JLabel licensePlateLabel = new JLabel("License Plate");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(licensePlateLabel, c);
		licensePlateField = new JTextField(15);
		licensePlateField.setText(t.getLicensePlate());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(licensePlateField, c);

		row = getLastRowIndex();
		JLabel producerLabel = new JLabel("Producer");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(producerLabel, c);
		producerField = new JTextField(15);
		producerField.setText(t.getProducer());
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(producerField, c);

		row = getLastRowIndex();
		JLabel costLabel = new JLabel("Cost");
		c.gridx = 0;
		c.gridy = row;
		getContentPane().add(costLabel, c);
		costField = new JTextField(15);
		costField.setText(t.getCost() + "");
		c.gridx = 1;
		c.gridy = row;
		getContentPane().add(costField, c);
	}

	@Override
	public GeneralBike getNewData() {
		t.setName(nameField.getText());
		t.setWeight(Float.valueOf(weightField.getText()));
		t.setLicensePlate(licensePlateField.getText());
		t.setProducer(producerField.getText());
		t.setCost(Float.valueOf(costField.getText()));

		return t;
	}
}
