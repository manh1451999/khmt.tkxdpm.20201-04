package com.ebr.components.generalbike.gui;

import java.util.Map;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ebr.components.abstractdatageneralbike.gui.ADataGeneralBikeSearchPane;

@SuppressWarnings("serial")
public class GeneralBikeSearchPane extends ADataGeneralBikeSearchPane {
	private JTextField nameField;
	private JTextField licensePlateField;
	private JTextField producerField;

	public GeneralBikeSearchPane() {
		super();
	}

	@Override
	public void buildControls() {
		JLabel nameLabel = new JLabel("Name");
		nameField = new JTextField(15);
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(nameLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(nameField, c);

		JLabel licensePlateLabel = new JLabel("License Plate");
		licensePlateField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(licensePlateLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(licensePlateField, c);

		JLabel producerLabel = new JLabel("Producer");
		producerField = new JTextField(15);
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		add(producerLabel, c);
		c.gridx = 1;
		c.gridy = row;
		add(producerField, c);
	}

	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();

		if (!nameField.getText().trim().equals("")) {
			res.put("name", nameField.getText().trim());
		}
		if (!licensePlateField.getText().trim().equals("")) {
			res.put("licensePlate", licensePlateField.getText().trim());
		}
		if (!producerField.getText().trim().equals("")) {
			res.put("producer", producerField.getText().trim());
		}

		return res;
	}
}
