package com.ebr.components.generalbike.twinbike.controller;

import java.util.List;
import java.util.Map;

import com.ebr.bean.GeneralBike;
import com.ebr.bean.TwinBike;
import com.ebr.components.generalbike.controller.AdminGeneralBikePageController;
import com.ebr.components.generalbike.gui.GeneralBikeSearchPane;
import com.ebr.components.generalbike.gui.GeneralBikeSinglePane;
import com.ebr.components.generalbike.twinbike.gui.TwinBikeSearchPane;
import com.ebr.components.generalbike.twinbike.gui.TwinBikeSinglePane;
import com.ebr.serverapi.GeneralBikeApi;

public class AdminTwinBikePageController extends AdminGeneralBikePageController {
	@Override
	public List<? extends GeneralBike> search(Map<String, String> searchParams) {
		return new GeneralBikeApi().getTwinBikes(searchParams);
	}

	@Override
	public GeneralBikeSinglePane createGeneralBikeSinglePane() {
		return new TwinBikeSinglePane();
	}

	@Override
	public GeneralBikeSearchPane createGeneralBikeSearchPane() {
		return new TwinBikeSearchPane();
	}

	@Override
	public GeneralBike updateGeneralBike(GeneralBike generalBike) {
		return new GeneralBikeApi().updateTwinBike((TwinBike) generalBike);
	}
}
