package com.ebr.components.generalbike.twinbike.gui;

import com.ebr.bean.GeneralBike;
import com.ebr.components.generalbike.gui.GeneralBikeSinglePane;

@SuppressWarnings("serial")
public class TwinBikeSinglePane extends GeneralBikeSinglePane {
	public TwinBikeSinglePane() {
		super();
	}

	public TwinBikeSinglePane(GeneralBike generalBike) {
		this();
		this.t = generalBike;

		displayDataGeneralBike();
	}

	@Override
	public void buildControls() {
		super.buildControls();
	}

	@Override
	public void displayDataGeneralBike() {
		super.displayDataGeneralBike();
	}
}
