package com.ebr.components.generalbike.bike.gui;

import com.ebr.bean.GeneralBike;
import com.ebr.components.generalbike.gui.GeneralBikeSinglePane;

@SuppressWarnings("serial")
public class BikeSinglePane extends GeneralBikeSinglePane {
	public BikeSinglePane() {
		super();
	}

	public BikeSinglePane(GeneralBike generalBike) {
		this();
		this.t = generalBike;

		displayDataGeneralBike();
	}

	@Override
	public void buildControls() {
		super.buildControls();
	}

	@Override
	public void displayDataGeneralBike() {
		super.displayDataGeneralBike();
	}
}
