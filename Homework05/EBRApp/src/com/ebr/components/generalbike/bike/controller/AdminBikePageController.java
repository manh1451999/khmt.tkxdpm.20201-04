package com.ebr.components.generalbike.bike.controller;

import java.util.List;
import java.util.Map;

import com.ebr.bean.Bike;
import com.ebr.bean.GeneralBike;
import com.ebr.components.generalbike.bike.gui.BikeSearchPane;
import com.ebr.components.generalbike.bike.gui.BikeSinglePane;
import com.ebr.components.generalbike.controller.AdminGeneralBikePageController;
import com.ebr.components.generalbike.gui.GeneralBikeSearchPane;
import com.ebr.components.generalbike.gui.GeneralBikeSinglePane;
import com.ebr.serverapi.GeneralBikeApi;

public class AdminBikePageController extends AdminGeneralBikePageController {
	@Override
	public List<? extends GeneralBike> search(Map<String, String> searchParams) {
		return new GeneralBikeApi().getBikes(searchParams);
	}
	
	@Override
	public GeneralBikeSinglePane createGeneralBikeSinglePane() {
		return new BikeSinglePane();
	}
	
	@Override
	public GeneralBikeSearchPane createGeneralBikeSearchPane() {
		return new BikeSearchPane();
	}
	
	@Override
	public GeneralBike updateGeneralBike(GeneralBike generalBike) {
		return new GeneralBikeApi().updateBike((Bike) generalBike);
	}
}
