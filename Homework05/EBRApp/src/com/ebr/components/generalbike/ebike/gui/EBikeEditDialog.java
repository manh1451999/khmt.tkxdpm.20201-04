package com.ebr.components.generalbike.ebike.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import com.ebr.bean.EBike;
import com.ebr.bean.GeneralBike;
import com.ebr.components.abstractdatageneralbike.controller.IDataGeneralBikeManageController;
import com.ebr.components.generalbike.gui.GeneralBikeEditDialog;

@SuppressWarnings("serial")
public class EBikeEditDialog extends GeneralBikeEditDialog {
	private JTextField batteryPercentageField;
	private JTextField loadCyclesField;
	private JTextField estimatedUsageTimeRemainingField;

	public EBikeEditDialog(GeneralBike generalBike, IDataGeneralBikeManageController<GeneralBike> controller) {
		super(generalBike, controller);
	}

	@Override
	public void buildControls() {
		super.buildControls();

		if (t instanceof EBike) {
			EBike eBike = (EBike) t;

			int row = getLastRowIndex();
			JLabel batteryPercentageLabel = new JLabel("Battery Percentage");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(batteryPercentageLabel, c);
			batteryPercentageField = new JTextField(15);
			batteryPercentageField.setText(eBike.getBatteryPercentage() + "");
			c.gridx = 1;
			c.gridy = row;
			getContentPane().add(batteryPercentageField, c);

			row = getLastRowIndex();
			JLabel loadCyclesLabel = new JLabel("Load Cycles");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(loadCyclesLabel, c);
			loadCyclesField = new JTextField(15);
			loadCyclesField.setText(eBike.getLoadCycles() + "");
			c.gridx = 1;
			c.gridy = row;
			getContentPane().add(loadCyclesField, c);

			row = getLastRowIndex();
			JLabel estimatedUsageTimeRemainingLabel = new JLabel("Estimated Usage Time Remaining");
			c.gridx = 0;
			c.gridy = row;
			getContentPane().add(estimatedUsageTimeRemainingLabel, c);
			estimatedUsageTimeRemainingField = new JTextField(15);
			estimatedUsageTimeRemainingField.setText(eBike.getEstimatedUsageTimeRemaining() + "");
			c.gridx = 1;
			c.gridy = row;
			getContentPane().add(estimatedUsageTimeRemainingField, c);
		}
	}

	@Override
	public GeneralBike getNewData() {
		super.getNewData();
		
		if (t instanceof EBike) {
			EBike eBike = (EBike) t;
			
			eBike.setBatteryPercentage(Float.valueOf(batteryPercentageField.getText()));
			eBike.setLoadCycles(Integer.valueOf(loadCyclesField.getText()));
			eBike.setEstimatedUsageTimeRemaining(Integer.valueOf(estimatedUsageTimeRemainingField.getText()));
		}

		return t;
	}
}
