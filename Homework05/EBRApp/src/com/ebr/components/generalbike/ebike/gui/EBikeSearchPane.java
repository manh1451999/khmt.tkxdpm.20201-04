package com.ebr.components.generalbike.ebike.gui;

import java.util.Map;

import com.ebr.components.generalbike.gui.GeneralBikeSearchPane;

@SuppressWarnings("serial")
public class EBikeSearchPane extends GeneralBikeSearchPane {
	public EBikeSearchPane() {
		super();
	}
	
	@Override
	public void buildControls() {
		super.buildControls();
	}
	
	@Override
	public Map<String, String> getQueryParams() {
		Map<String, String> res = super.getQueryParams();
		
		return res;
	}
}
