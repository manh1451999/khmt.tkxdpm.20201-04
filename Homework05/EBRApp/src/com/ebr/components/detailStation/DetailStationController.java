package com.ebr.components.detailStation;

import java.util.List;

import javax.swing.JPanel;

import com.ebr.bean.Bike;





public class DetailStationController {
	
	public JPanel getSingleBikePage(List<Bike> list) {
		SingleBikePageController controller = new SingleBikePageController(list);
		return controller.getDataPagePane();
	}
	
	
}
