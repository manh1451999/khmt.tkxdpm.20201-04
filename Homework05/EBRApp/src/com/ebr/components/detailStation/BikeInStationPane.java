package com.ebr.components.detailStation;

import java.awt.BorderLayout;

import javax.swing.JPanel;
import javax.swing.SpringLayout;
import javax.swing.border.Border;

import com.ebr.components.abstractdata.gui.ADataListPane;
import com.ebr.components.abstractdata.gui.ADataSearchPane;

@SuppressWarnings("serial")
public class BikeInStationPane<T> extends JPanel{
	
	public BikeInStationPane(ADataListPane<T> listPane) {
		BorderLayout layout = new BorderLayout();
//		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
//		this.add(searchPane);
		this.add(listPane);
		
		
//		layout.putConstraint(SpringLayout.WEST, searchPane, 5, SpringLayout.WEST, this);
//		layout.putConstraint(SpringLayout.NORTH, searchPane, 5, SpringLayout.NORTH, this);
//		layout.putConstraint(SpringLayout.EAST, searchPane, -5, SpringLayout.EAST, this);
//		
		
//		layout.putConstraint(SpringLayout.WEST, listPane, 5, SpringLayout.WEST, this);
////		layout.putConstraint(SpringLayout.NORTH, listPane, 5, SpringLayout.SOUTH, this);
//		layout.putConstraint(SpringLayout.EAST, listPane, -5, SpringLayout.EAST, this);
//		layout.putConstraint(SpringLayout.SOUTH, listPane, -5, SpringLayout.SOUTH, this);
		layout.addLayoutComponent(listPane,BorderLayout.CENTER);
	}
	
	public BikeInStationPane(ADataSearchPane searchPane, ADataListPane<T> listPane) {
		SpringLayout layout = new SpringLayout();
		this.setLayout(layout);
		
		
		this.add(searchPane);
		this.add(listPane);
		
		
		layout.putConstraint(SpringLayout.WEST, searchPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, searchPane, 5, SpringLayout.NORTH, this);
		layout.putConstraint(SpringLayout.EAST, searchPane, -5, SpringLayout.EAST, this);
		
		
		layout.putConstraint(SpringLayout.WEST, listPane, 5, SpringLayout.WEST, this);
		layout.putConstraint(SpringLayout.NORTH, listPane, 5, SpringLayout.SOUTH, searchPane);
		layout.putConstraint(SpringLayout.EAST, listPane, -5, SpringLayout.EAST, this);
		layout.putConstraint(SpringLayout.SOUTH, listPane, -5, SpringLayout.SOUTH, this);
	}
	
}