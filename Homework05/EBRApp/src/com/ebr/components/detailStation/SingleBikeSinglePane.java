package com.ebr.components.detailStation;

import java.util.Date;

import javax.swing.JLabel;

import com.ebr.bean.Bike;
import com.ebr.components.abstractdata.gui.ADataSinglePane;



public class SingleBikeSinglePane extends ADataSinglePane<Bike>{
	

	private JLabel labelName;
	private JLabel labelLicensePlates;
	private JLabel labelCost;
	private JLabel labelManuafacturingDate;
	private JLabel labelProducer;
	private JLabel labelWeight;
	
	@Override
	public void buildControls() {
		// TODO Auto-generated method stub
		super.buildControls();
		
		int row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelName = new JLabel();
		add(labelName, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelLicensePlates = new JLabel();
		add(labelLicensePlates, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelCost = new JLabel();
		add(labelCost, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelWeight = new JLabel();
		add(labelWeight, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelManuafacturingDate = new JLabel();
		add(labelManuafacturingDate, c);
		
		row = getLastRowIndex();
		c.gridx = 0;
		c.gridy = row;
		labelProducer = new JLabel();
		add(labelProducer, c);
	}
	
	@Override
	public void displayData() {
		// TODO Auto-generated method stub
		labelName.setText("Name: " + t.getName());
		labelLicensePlates.setText("LicensePlates: " + t.getLicensePlate());
		labelCost.setText("Cost per 15 minutes: " + t.getCost() + "");
		labelWeight.setText("Retainer fee: "+t.getWeight());
		labelManuafacturingDate.setText("Seate: " + t.getManuafacturingDate()+ "");
		labelProducer.setText("Cost: " + t.getCost() + "");
	}

}
