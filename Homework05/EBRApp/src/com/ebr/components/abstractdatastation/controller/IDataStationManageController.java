package com.ebr.components.abstractdatastation.controller;

public interface IDataStationManageController<T> {
	public String create(T t);
	public void read(T t);
	public void delete(T t);
	public void update(T t);
}
