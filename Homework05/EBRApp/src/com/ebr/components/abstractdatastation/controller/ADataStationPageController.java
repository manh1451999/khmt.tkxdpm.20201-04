package com.ebr.components.abstractdatastation.controller;

import java.util.List;
import java.util.Map;

import javax.swing.JPanel;

import com.ebr.components.abstractdatastation.gui.ADataStationListPane;
import com.ebr.components.abstractdatastation.gui.ADataStationPagePane;
import com.ebr.components.abstractdatastation.gui.ADataStationSearchPane;
import com.ebr.components.abstractdatastation.gui.ADataStationSinglePane;

public abstract class ADataStationPageController<T> {
	private ADataStationPagePane<T> stationPagePane;
	
	public ADataStationPageController() {
		ADataStationSearchPane stationSearchPane = createStationSearchPane();
		
		ADataStationListPane<T> stationListPane = createStationListPane();
		
		stationSearchPane.setController(new IDataStationSearchController() {
			@Override
			public void search(Map<String, String> searchParams) {
				List<? extends T> list = ADataStationPageController.this.search(searchParams);
				stationListPane.updateDataStation(list);
			}
		});
		
		stationSearchPane.fireSearchEvent();
		
		stationPagePane = new ADataStationPagePane<T>(stationSearchPane, stationListPane);
	}
	
	public JPanel getDataStationPagePane() {
		return stationPagePane;
	}
	
	
	public abstract ADataStationSearchPane createStationSearchPane();

	public abstract List<? extends T> search(Map<String, String> searchParams);
	
	public abstract ADataStationSinglePane<T> createStationSinglePane();
	
	public abstract ADataStationListPane<T> createStationListPane();
}
