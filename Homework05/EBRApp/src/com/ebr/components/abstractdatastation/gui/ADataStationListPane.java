package com.ebr.components.abstractdatastation.gui;

import java.awt.Dimension;
import java.awt.LayoutManager;
import java.util.List;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import com.ebr.components.abstractdatastation.controller.ADataStationPageController;

@SuppressWarnings("serial")
public abstract class ADataStationListPane<T> extends JScrollPane {
	private LayoutManager layout;
	protected JPanel pane;
	
	protected ADataStationPageController<T> controller;
	
	public ADataStationListPane() {
		pane = new JPanel();
		layout = new BoxLayout(pane, BoxLayout.Y_AXIS);
		pane.setLayout(layout);
		
		this.setViewportView(pane);
		this.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		this.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		this.getVerticalScrollBar().setUnitIncrement(20);
		this.getHorizontalScrollBar().setUnitIncrement(20);
	}
	
	public abstract void decorateStationSinglePane(ADataStationSinglePane<T> singlePane);

	public void updateDataStation(List<? extends T> list) {
		pane.removeAll();
		pane.revalidate();
		pane.repaint();
		
		for (T t: list) {
			ADataStationSinglePane<T> stationSinglePane = controller.createStationSinglePane();
			decorateStationSinglePane(stationSinglePane);
	        
			stationSinglePane.updateDataStation(t);
            pane.add(stationSinglePane);
            pane.add(Box.createRigidArea(new Dimension(0, 40)));
        }
	}
}
