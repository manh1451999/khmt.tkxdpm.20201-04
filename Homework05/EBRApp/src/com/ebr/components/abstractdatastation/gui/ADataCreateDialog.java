package com.ebr.components.abstractdatastation.gui;

import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import com.ebr.bean.Station;
import com.ebr.components.abstractdatastation.controller.IDataStationManageController;
//import com.oms.components.abstractdata.gui.ADataEditDialog;

public abstract class ADataCreateDialog<T> extends JDialog {
	protected T t;
	protected GridBagLayout layout;
	protected GridBagConstraints c = new GridBagConstraints();

	public ADataCreateDialog( IDataStationManageController controller) {
		super((Frame) null, "Add", true);
		
		this.t = t;
		
		setContentPane(new JPanel());
		layout = new GridBagLayout();
		getContentPane().setLayout(layout);

		
		this.buildControls();
		
		JButton saveButton = new JButton("Save");
		saveButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
			 Map<String, String> map= getDataCreate();
				System.out.println("newwwww"+ map);
				Station station = new Station();
				station.setName(map.get("name"));
				station.setAddress(map.get("address"));
				if (map.get("numberOfEmptyDocks").equals(""))
						station.setNumberOfEmptyDocks(0);
				else if((Integer.parseInt(map.get("numberOfEmptyDocks"))) == (int)(Integer.parseInt(map.get("numberOfEmptyDocks"))))
				station.setNumberOfEmptyDocks(Integer.parseInt(map.get("numberOfEmptyDocks")));
				else
					station.setNumberOfEmptyDocks(0);
				System.out.println("obbbbb"+station );
				controller.create(station);
				
				ADataCreateDialog.this.dispose();
			}
		});
		
		
		c.gridx = 1;
		c.gridy = getLastRowIndex();
		getContentPane().add(saveButton, c);
		
		
		this.pack();
		this.setResizable(false);
		this.setVisible(true);
	}
	
	protected int getLastRowIndex() {
		layout.layoutContainer(getContentPane());
		int[][] dim = layout.getLayoutDimensions();
	    int rows = dim[1].length;
	    return rows;
	}
	
	public abstract void buildControls();
	
	public abstract T getNewData();
	public abstract Map<String, String> getDataCreate();
}