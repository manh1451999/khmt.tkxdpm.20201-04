package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.GeneralBike;
import com.ebr.bean.TwinBike;
import com.ebr.db.IGeneralBikeDatabase;
import com.ebr.db.JsonGeneralBikeDatabase;

@Path("/twinbikes")
public class TwinBikeService {
	private IGeneralBikeDatabase generalBikeDatabase;

	public TwinBikeService() {
		generalBikeDatabase = JsonGeneralBikeDatabase.singleton();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<GeneralBike> getBikes(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("licensePlate") String licensePlate, @QueryParam("producer") String producer) {
		TwinBike twinBike = new TwinBike(id, name, "twinbike", 0, licensePlate, null, producer, 0);
		ArrayList<GeneralBike> res = generalBikeDatabase.searchGeneralBike(twinBike);
		return res;
	}

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public GeneralBike updateBike(@PathParam("id") String id, TwinBike twinBike) {
		return generalBikeDatabase.updateGeneralBike(twinBike);
	}
}
