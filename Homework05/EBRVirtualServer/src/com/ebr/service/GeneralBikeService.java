package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.GeneralBike;
import com.ebr.db.IGeneralBikeDatabase;
import com.ebr.db.JsonGeneralBikeDatabase;

@Path("/generalbikes")
public class GeneralBikeService {
	private IGeneralBikeDatabase generalBikeDatabase;

	public GeneralBikeService() {
		generalBikeDatabase = JsonGeneralBikeDatabase.singleton();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<GeneralBike> getAllGeneralBikes() {
		ArrayList<GeneralBike> res = generalBikeDatabase.searchGeneralBike(null);
		return res;
	}
}
