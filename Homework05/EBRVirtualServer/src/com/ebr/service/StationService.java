package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.Bike;
import com.ebr.bean.GeneralBike;
import com.ebr.bean.Station;
import com.ebr.db.IStationDatabase;
import com.ebr.db.JsonStationDatabase;

@Path("/stations")
public class StationService {
	private IStationDatabase stationDatabase;

	public StationService() {
		stationDatabase = JsonStationDatabase.singleton();
	}

	@GET
	@Path("/")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Station> getAllStations() {
		ArrayList<Station> res = stationDatabase.searchStation(null);
		return res;
	}
	
	@GET
	@Path("/search")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Station> getStations(@QueryParam("id") String id, @QueryParam("name") String name,
			@QueryParam("address") String address, @QueryParam("numberOfEmptyDocks") int numberOfEmptyDocks) {
		
		Station station = new Station(id, name, address,null, null, null,  numberOfEmptyDocks, 0, 0);
		
		ArrayList<Station> res = stationDatabase.searchStation(station);
		
		return res;
	}

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Station updateStation(@PathParam("id") String id, Station station) {
		return stationDatabase.updateStation(station);
	}
	@POST
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public String addStation(Station station) {
		//stationDatabase.addStation(station);
		String res =  stationDatabase.addStation(station);
		String result = new String(res);
		return result;
	}
}
