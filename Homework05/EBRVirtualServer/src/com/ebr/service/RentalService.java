package com.ebr.service;

import java.util.ArrayList;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.ebr.bean.Rental;
import com.ebr.bean.Station;
import com.ebr.db.IRentalDatabase;
import com.ebr.db.IStationDatabase;
import com.ebr.db.JsonRentalDatabase;
import com.ebr.db.JsonStationDatabase;

@Path("/rentals")
public class RentalService {
	private IRentalDatabase rentalDatabase;

	public RentalService() {
		rentalDatabase = JsonRentalDatabase.singleton();
	}

	@GET
	@Path("")
	@Produces(MediaType.APPLICATION_JSON)
	public ArrayList<Rental> getAllStations() {
		ArrayList<Rental> res = rentalDatabase.searchRental(null);
		return res;
	}

	@POST
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Rental updateStation(@PathParam("id") String id, Rental rental) {
		return rentalDatabase.updateRental(rental);
	}
}
