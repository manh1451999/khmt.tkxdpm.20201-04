package com.ebr.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonTypeName("generalbike")
@JsonSubTypes({ @Type(value = Bike.class, name = "bike"), @Type(value = EBike.class, name = "ebike"),
		@Type(value = TwinBike.class, name = "twinbike") })
public class GeneralBike {
	private String id;
	private String name;
	private String bikeType;
	private float weight;
	private String licensePlate;
	private Date manuafacturingDate;
	private String producer;
	private float cost;

	public GeneralBike() {
		super();
	}

	public GeneralBike(String id, String name, String bikeType, float weight, String licensePlate,
			Date manuafacturingDate, String producer, float cost) {
		super();
		this.id = id;
		this.name = name;
		this.bikeType = bikeType;
		this.weight = weight;
		this.licensePlate = licensePlate;
		this.manuafacturingDate = manuafacturingDate;
		this.producer = producer;
		this.cost = cost;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBikeType() {
		return bikeType;
	}

	public void setBikeType(String bikeType) {
		this.bikeType = bikeType;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public String getLicensePlate() {
		return licensePlate;
	}

	public void setLicensePlate(String licensePlate) {
		this.licensePlate = licensePlate;
	}

	public Date getManuafacturingDate() {
		return manuafacturingDate;
	}

	public void setManuafacturingDate(Date manuafacturingDate) {
		this.manuafacturingDate = manuafacturingDate;
	}

	public String getProducer() {
		return producer;
	}

	public void setProducer(String producer) {
		this.producer = producer;
	}

	public float getCost() {
		return cost;
	}

	public void setCost(float cost) {
		this.cost = cost;
	}

	public boolean match(GeneralBike generalBike) {
		if (generalBike == null) {
			return true;
		}

		if (generalBike.id != null && !generalBike.id.equals("") && !this.id.contains(generalBike.id)) {
			return false;
		}
		if (generalBike.name != null && !generalBike.name.equals("") && !this.name.contains(generalBike.name)) {
			return false;
		}
		if (generalBike.bikeType != null && !generalBike.bikeType.equals("")
				&& !this.bikeType.contains(generalBike.bikeType)) {
			return false;
		}
		if (generalBike.weight != 0 && this.weight != generalBike.weight) {
			return false;
		}
		if (generalBike.licensePlate != null && !generalBike.licensePlate.equals("")
				&& !this.licensePlate.contains(generalBike.licensePlate)) {
			return false;
		}
		if (generalBike.manuafacturingDate != null && !this.manuafacturingDate.equals(generalBike.manuafacturingDate)) {
			return false;
		}
		if (generalBike.producer != null && !generalBike.producer.equals("")
				&& !this.producer.contains(generalBike.producer)) {
			return false;
		}
		if (generalBike.cost != 0 && this.cost != generalBike.cost) {
			return false;
		}

		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof GeneralBike) {
			return this.id.equals(((GeneralBike) obj).id);
		}
		return false;
	}
}
