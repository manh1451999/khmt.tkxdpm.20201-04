package com.ebr.bean;

import java.util.Date;

public class Bike extends GeneralBike {
	public Bike() {
		super();
	}

	public Bike(String id, String name, String bikeType, float weight, String licensePlate, Date manuafacturingDate,
			String producer, float cost) {
		super(id, name, bikeType, weight, licensePlate, manuafacturingDate, producer, cost);
	}
}
