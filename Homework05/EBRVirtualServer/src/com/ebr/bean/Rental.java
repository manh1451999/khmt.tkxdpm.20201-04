package com.ebr.bean;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeName;

//@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
//@JsonTypeName("rental")
public class Rental {

	private String id;
	private String idBaiXe;
	private String loaiXe;
	private String maXe;
	private float tienCoc;
	private float thoiGianThue;
	private String thoiGianBatDauThue;
	private float tienTra;
	
	public Rental() {
		// TODO Auto-generated constructor stub
		super();
	}

	public Rental(
			String id, 
			
			String idBaiXe,
			
			String loaiXe, 
			String maXe,
			float tienCoc,

			float thoiGianThue,
			String thoiGianBatDauThue,
			float tienTra
			) {
		super();
		this.id = id;
		this.idBaiXe = idBaiXe;
		this.loaiXe = loaiXe; 
		this.maXe = maXe;
		this.tienCoc = tienCoc;

		this.thoiGianThue = thoiGianThue;
		this.thoiGianBatDauThue = thoiGianBatDauThue;
		this.tienTra = tienTra;
	}

	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getMaXe() {
		return maXe;
	}
	
	public void setMaXe(String maXe) {
		this.maXe = maXe;
	}

	public String getLoaiXe() {
		return loaiXe;
	}
	
	public void setLoaiXe(String loaiXe) {
		this.loaiXe = loaiXe;
	}

	public String getIdBaiXe() {
		return idBaiXe;
	}
	
	public void setIdBaiXe(String idBaiXe) {
		this.idBaiXe = idBaiXe;
	}
	

	public float getTienCoc() {
		return tienCoc;
	}
	
	public void setTienCoc(float tienCoc) {
		this.tienCoc = tienCoc;
	}

	public String getThoiGianBatDauThue() {
		return thoiGianBatDauThue;
	}
	
	public void setThoiGianBatDauThue(String thoiGianBatDauThue) {
		this.thoiGianBatDauThue = thoiGianBatDauThue;
	}

	public float getThoiGianThue() {
		return thoiGianThue;
	}
	
	public void setThoiGianThue(float thoiGianThue) {
		this.thoiGianThue = thoiGianThue;
	}
	
	public float getTienTra () {
		return tienTra;
	}
	
	public void setTienTra ( float tienTra) {
		this.tienTra = tienTra;
	}

	
	public boolean match(Rental rental) {
		if (rental == null) {
			return true;
		}

		if (rental.id != null && !rental.id.equals("") && !this.id.contains(rental.id)) {
			return false;
		}

		return true;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Rental) {
			return this.id.equals(((Rental) obj).id);
		}
		return false;
	}
}