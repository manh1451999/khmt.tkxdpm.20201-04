package com.ebr.db;

import java.util.ArrayList;

import com.ebr.bean.Station;
import com.ebr.db.seed.Seed;

public class JsonStationDatabase implements IStationDatabase {
	private static IStationDatabase singleton = new JsonStationDatabase();

	private ArrayList<Station> stations = Seed.singleton().getStations();

	private JsonStationDatabase() {
	}

	public static IStationDatabase singleton() {
		return singleton;
	}

	@Override
	public ArrayList<Station> searchStation(Station station) {
		ArrayList<Station> res = new ArrayList<Station>();
		for (Station x : stations) {
			if (x.match(station)) {
				res.add(x);
			}
		}
		return res;
	}

	@Override
	public Station updateStation(Station station) {
		for (Station x : stations) {
			if (x.equals(station)) {
				stations.remove(x);
				stations.add(station);
				return station;
			}
		}
		return null;
	}


	@Override
	public String addStation(Station station) {
		int length = stations.size();
		length++;
		String id= "station" + String.valueOf(length);
		station.setId(id);
		if(station.getAddress().equals("") || station.getName().equals(""))
			return "Can not create empty field !";
//		else if(station.getNumberOfEmptyDocks() == (int )station.getNumberOfEmptyDocks())
//			return "Number of empty docks must be type integer!";
		for (Station x : stations) {
			System.out.println(x.getAddress() + " " + x.getName() + " ");
			if (x.getName().equals(station.getName()) || x.getAddress().equals(station.getAddress())) {
				//stations.remove(x);
				
				return "Station exist. Add station faile";
			}
			
		}
		stations.add(station);
		return "Add station success ! ";
	}
}
