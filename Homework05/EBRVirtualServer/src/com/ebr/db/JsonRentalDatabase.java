package com.ebr.db;

import java.util.ArrayList;

import com.ebr.bean.Rental;
import com.ebr.bean.Station;
import com.ebr.db.seed.Seed;

public class JsonRentalDatabase implements IRentalDatabase {
	private static IRentalDatabase singleton = new JsonRentalDatabase();

	private ArrayList<Rental> rentals = Seed.singleton().getRentals();

	private JsonRentalDatabase() {
	}

	public static IRentalDatabase singleton() {
		return singleton;
	}

	@Override
	public ArrayList<Rental> searchRental(Rental station) {
		ArrayList<Rental> res = new ArrayList<Rental>();
		for (Rental x : rentals) {
			if (x.match(station)) {
				res.add(x);
			}
		}
		return res;
	}

	@Override
	public Rental updateRental(Rental rental) {
		for (Rental x : rentals) {
			if (x.equals(rental)) {
				rentals.remove(x);
				rentals.add(rental);
				return rental;
			}
		}
		return null;
	}
}
