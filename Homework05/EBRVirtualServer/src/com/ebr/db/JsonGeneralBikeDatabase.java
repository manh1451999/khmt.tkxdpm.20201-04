package com.ebr.db;

import java.util.ArrayList;

import com.ebr.bean.GeneralBike;
import com.ebr.db.seed.Seed;

public class JsonGeneralBikeDatabase implements IGeneralBikeDatabase {
	private static IGeneralBikeDatabase singleton = new JsonGeneralBikeDatabase();

	private ArrayList<GeneralBike> generalBikes = Seed.singleton().getGeneralBikes();

	private JsonGeneralBikeDatabase() {
	}

	public static IGeneralBikeDatabase singleton() {
		return singleton;
	}

	@Override
	public ArrayList<GeneralBike> searchGeneralBike(GeneralBike generalBike) {
		ArrayList<GeneralBike> res = new ArrayList<GeneralBike>();
		for (GeneralBike x : generalBikes) {
			if (x.match(generalBike)) {
				res.add(x);
			}
		}
		return res;
	}

	@Override
	public GeneralBike updateGeneralBike(GeneralBike generalBike) {
		for (GeneralBike x : generalBikes) {
			if (x.equals(generalBike)) {
				generalBikes.remove(x);
				generalBikes.add(generalBike);
				return generalBike;
			}
		}
		return null;
	}
}
