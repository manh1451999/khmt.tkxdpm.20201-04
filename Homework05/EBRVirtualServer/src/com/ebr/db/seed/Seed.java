package com.ebr.db.seed;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import com.ebr.bean.GeneralBike;
import com.ebr.bean.Rental;
import com.ebr.bean.Station;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Seed {
	private ArrayList<GeneralBike> generalBikes;
	private ArrayList<Station> stations;
	private ArrayList<Rental> rentals;

	private static Seed singleton = new Seed();

	private Seed() {
		start();
	}

	public static Seed singleton() {
		return singleton;
	}

	private void start() {
		generalBikes = new ArrayList<GeneralBike>();

		generalBikes.addAll(
				generateGeneralBikeDataFromFile(new File(getClass().getResource("./bikes.json").getPath()).toString()));
		generalBikes.addAll(generateGeneralBikeDataFromFile(
				new File(getClass().getResource("./ebikes.json").getPath()).toString()));
		generalBikes.addAll(generateGeneralBikeDataFromFile(
				new File(getClass().getResource("./twinbikes.json").getPath()).toString()));

		stations = new ArrayList<Station>();

		stations.addAll(
				generateStationDataFromFile(new File(getClass().getResource("./stations.json").getPath()).toString()));

		rentals = new ArrayList<Rental>();

		rentals.addAll(
				generateRentalDataFromFile(new File(getClass().getResource("./rentals.json").getPath()).toString()));
	}

	private ArrayList<? extends GeneralBike> generateGeneralBikeDataFromFile(String filePath) {
		ArrayList<? extends GeneralBike> res = new ArrayList<GeneralBike>();
		ObjectMapper mapper = new ObjectMapper();

		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<GeneralBike>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}

		return res;
	}

	private ArrayList<Station> generateStationDataFromFile(String filePath) {
		ArrayList<Station> res = new ArrayList<Station>();
		ObjectMapper mapper = new ObjectMapper();

		String json = FileReader.read(filePath);
		try {
			// mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Station>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from " + filePath);
		}

		return res;
	}

	private ArrayList<Rental> generateRentalDataFromFile(String filePath) {
		ArrayList<Rental> res = new ArrayList<Rental>();
		ObjectMapper mapper = new ObjectMapper();

		String json = FileReader.read(filePath);
		try {
			mapper.setDateFormat(new SimpleDateFormat("dd/MM/yyyy"));
			res = mapper.readValue(json, new TypeReference<ArrayList<Rental>>() {
			});
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("Invalid JSON input data from rental " + filePath);
		}

		return res;
	}

	public ArrayList<GeneralBike> getGeneralBikes() {
		return generalBikes;
	}

	public ArrayList<Station> getStations() {
		return stations;
	}
	
	public ArrayList<Rental> getRentals() {
		return rentals;
	}


	public static void main(String[] args) {
		Seed seed = new Seed();
		seed.start();
	}
}
