package com.ebr.db;

import java.util.ArrayList;

import com.ebr.bean.GeneralBike;

public interface IGeneralBikeDatabase {
	public ArrayList<GeneralBike> searchGeneralBike(GeneralBike generalBike);

	public GeneralBike updateGeneralBike(GeneralBike generalBike);
}
