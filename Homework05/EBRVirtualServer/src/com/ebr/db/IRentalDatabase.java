package com.ebr.db;

import java.util.ArrayList;

import com.ebr.bean.Rental;
import com.ebr.bean.Station;

public interface IRentalDatabase {
	public ArrayList<Rental> searchRental(Rental rental);

	public Rental updateRental(Rental rental);
}
