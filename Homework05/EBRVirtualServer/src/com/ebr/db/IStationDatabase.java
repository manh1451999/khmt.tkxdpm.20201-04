package com.ebr.db;

import java.util.ArrayList;

import com.ebr.bean.Station;

public interface IStationDatabase {
	public ArrayList<Station> searchStation(Station station);

	public Station updateStation(Station station);

	public String addStation(Station station);
}
